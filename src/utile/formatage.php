<?php

    function formatTitrePageH3($text, $color)
    {
        $chain = '<h3 class="perso_shadowTitre '.$color.' perso_policeTitre text-center mt-4">'.$text.'</h3>';
        return $chain;
    }

    function formatTitrePageH2($text, $color)
    {
        $chain = '<h2 class="perso_shadowTitre '.$color.' perso_policeTitre text-center mt-4">'.$text.'</h2>';
        return $chain;
    }

    function formatTitrePageH1($text, $color)
    {
        $chain = '<h1 class="perso_shadowTitre '.$color.' perso_policeTitre text-center mt-4">'.$text.'</h1>';
        return $chain;
    }

    function formatTitrePost($text)
    {
        $chain = '<h3 class="perso_shadowTitre border-bottom border-dark perso_policeTitre mt-5">'.$text.'</h3>';
        return $chain;
    }



?>