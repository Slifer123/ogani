<?php
    use Site\Entity\Personne;
    use Site\Entity\Article;
    use Site\Entity\Image;

    function getPersonneByEmailAndPassword($personneRepo,$email,$password)
    {
       $personneByEmailAndPassword = $personneRepo->findBy(["email_personne" => $email, 
                                "mdp_personne" => $password
                              ]);
        return $personneByEmailAndPassword;
    }

    function getPersonneByEmail($personneRepo,$email)
    {
       $personneByEmail = $personneRepo->findBy(["email_personne" => $email
                              ]);
        return $personneByEmail;
    }

    function getAllAboutAPerson($entityManager,$idPersonne)
    {
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('p')
        ->from(Personne::class, 'p')
        ->join(Article::class,'a')
        ->join(Image::class,'i')
        ->where('p.id_personne = :id_personne')
        ->setParameter('id_personne', $idPersonne);
        
        
        $query = $queryBuilder->getQuery();
        return $query;
    }

    function getAllAboutAPersonWithSpecificArticle($entityManager,$idPersonne,$idArticle)
    {
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('p','a','i')
        ->from(Personne::class, 'p')
        ->leftJoin('p.articles','a')
        ->leftJoin('a.images','i')
        ->where('p.id_personne = :id_personne')
        ->andWhere('a.id_article = :id_article')
        ->setParameter('id_personne', $idPersonne)
        ->setParameter('id_article', $idArticle);
        
        
        $query = $queryBuilder->getQuery();
        return $query;
    }

    function getAllAboutSpecificArticle($entityManager,$idArticle)
    {
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('p','a','i')
        ->from(Personne::class, 'p')
        ->leftJoin('p.articles','a')
        ->leftJoin('a.images','i')
        ->andWhere('a.id_article = :id_article')
        ->setParameter('id_article', $idArticle);
        
        
        $query = $queryBuilder->getQuery();
        return $query;
    }

    function getAllAboutASpecificPerson($entityManager,$idArticle)
    {
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('p','a','i')
        ->from(Personne::class, 'p')
        ->leftJoin('p.articles','a')
        ->leftJoin('a.images','i')
        ->andWhere('p.id_personne = :id_personne')
        ->setParameter('id_personne', $idArticle);
        
        
        $query = $queryBuilder->getQuery();
        return $query;
    }

   

    function getAllAboutAnArticle($entityManager,$idArticle)
    {
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('a')
        ->from(Article::class,'a')
        ->where('a.id_article = :id_article')
        ->setParameter('id_article', $idArticle);
        
        
        $query = $queryBuilder->getQuery();
        return $query;
    }

    function getAllAllArticlesOfAPerson($entityManager,$idPersonne)
    {
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('a')
        ->from(Article::class,'a')
        ->where('a.personne = :personne')
        ->setParameter('personne', $idPersonne);
        
        
        $query = $queryBuilder->getQuery();
        return $query;
    }

    function getAllArticlesThatContainsASpecificValue($entityManager,$value,$premier,$parPage)
    {
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('a')
        ->from(Article::class, 'a')
        ->addOrderBy('a.id_article', 'DESC')
        ->where('a.titre_article LIKE :recherche')
        ->setParameter('recherche', $value)
        ->setFirstResult($premier)
        ->setMaxResults($parPage);
        
        
        $query = $queryBuilder->getQuery();
        return $query;
    }

    function getAllArticlesWithASpecificCategorie($entityManager,$value,$premier,$parPage)
    {
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('a')
        ->from(Article::class, 'a')
        ->addOrderBy('a.id_article', 'DESC')
        ->where('a.categorie_article LIKE :categorie')
        ->setParameter('categorie', $value)
        ->setFirstResult($premier)
        ->setMaxResults($parPage);
        
        
        $query = $queryBuilder->getQuery();
        return $query;
    }

    function getTotalNumberOfAllArticles($entityManager)
    {
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('count(a)')
        ->from(Article::class, 'a');
        
        
        $query = $queryBuilder->getQuery();
        return $query;
    }

    function getTotalNumberOfAllArticlesByCategorie($entityManager, $categorie)
    {
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('count(a)')
        ->from(Article::class, 'a')
        ->where('a.categorie_article LIKE :categorie')
        ->setParameter('categorie', $categorie);
        
        
        $query = $queryBuilder->getQuery();
        return $query;
    }




?>