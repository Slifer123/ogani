<?php
namespace Site\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection; 

/**
* @ORM\Entity
*@ORM\Table(name="article")
*/


class Article
{
    // Variables membres

    /**
    * @ORM\Id
    * @ORM\GeneratedValue
    * @ORM\Column(type="integer")
    */
    private $id_article;

    /**
    * @ORM\Column(type="string")
    */
    private $titre_article;

    /**
    * @ORM\Column(type="string")
    */
    private $description_article;

    /**
    * @ORM\Column(type="string")
    */
    private $categorie_article;

    /**
    * @ORM\Column(type="string")
    */
    private $etat_article;

    /**
    * @ORM\Column(type="date")
    */
    private $date_achat_article;

    /**
    * @ORM\Column(type="integer")
    */
    private $prix_achat_article;

    /**
    * @ORM\Column(type="integer")
    */
    private $prix_vente_article;

    /**
    * @ORM\Column(type="string")
    */
    private $preuve_achat;

    /**
     * One article has many images. This is the inverse side.
     * @ORM\OneToMany(targetEntity=Image::class, cascade={"persist", "remove"}, mappedBy="article")
     */
    private $images;

    /**
    * One person can add many artciles
    * @ORM\ManyToOne(targetEntity=Personne::class,inversedBy="articles")
    * @ORM\JoinColumn(name="id_personne", referencedColumnName="id_personne")
    */
    protected $personne;

    function __construct($titre,$description,$categorie,$etat,$date_achat,$prix_achat,$prix_vente,$preuve_achat) {
        $this->titre_article = $titre;
        $this->description_article = $description;
        $this->categorie_article = $categorie;
        $this->etat_article = $etat;
        $this->date_achat_article = $date_achat;
        $this->prix_achat_article = $prix_achat;
        $this->prix_vente_article = $prix_vente;
        $this->preuve_achat = $preuve_achat;
        $this->images = new ArrayCollection();
    }

    /*public function __construct()
    {
        $this->images = new ArrayCollection();
    }*/

    // Fonctions membres
    //getters
    public function getId()
    {
        return $this->id_article ;
    }

    public function getTitre()
    {
        return $this->titre_article ;
    }

    public function getDescription()
    {
        return $this->description_article ;
    }

    public function getCategorie()
    {
        return $this->categorie_article ;
    }

    public function getEtat()
    {
        return $this->etat_article;
    }

    public function getDateAchat()
    {
        return $this->date_achat_article;
    }

    public function getPrixAchat()
    {
        return $this->prix_achat_article;
    }

    public function getPrixVente()
    {
        return $this->prix_vente_article;
    }

    public function getImages()
    {
        return $this->images;
    }

    public function getPersonne()
    {
        return $this->personne;
    }
     
    public function getPreuveAchat()
    {
        return $this->preuve_achat;
    }
    
    //setters
    public function setTitre($titre)
    {
        $this->titre_article = $titre;
    }

    public function setDescription($description)
    {
        $this->description_article = $description;
    }

    public function setCategorie($categorie)
    {
        $this->categorie_article = $categorie;
    }

    public function setEtat($etat)
    {
        $this->etat_article = $etat;
    }

    public function setDateAchat($date_achat)
    {
        $this->date_achat_article = $date_achat;
    }

    public function setPrixAchat($prix_achat)
    {
        $this->prix_achat_article = $prix_achat;
    }

    public function setPrixVente($prix_vente)
    {
        $this->prix_vente_article = $prix_vente;
    }

    public function setPersonne($personne)
    {
        $this->personne = $personne;
    }

    public function setPreuveAchat($preuve_achat)
    {
        $this->preuve_achat = $preuve_achat;
    }

    public function addImage(Image $image)
    {
        $this->images->add($image);
        $image->setArticle($this);
    }

    public function __toString()
    {
        $format = "Article (id: %s, titre: %s, description: %s, categorie: %s, etat: %s, dateAchat: %s, prixAchat: %s, prixVente: %s)\n";
        return sprintf($format, $this->id_article, $this->titre_article, $this->description_article, $this->categorie_article, $this->etat_article, $this->date_achat_article, $this->prix_achat_article, $this->prix_vente_article);
    }
    
}