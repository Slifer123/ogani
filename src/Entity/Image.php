<?php
namespace Site\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
*@ORM\Table(name="images")
*/

class Image
{
    // Variables membres

    /**
    * @ORM\Id
    * @ORM\GeneratedValue
    * @ORM\Column(type="integer")
    */
    private $id_image;

    /**
    * @ORM\Column(type="string")
    */
    private $url_image;

    /**
    * many images can be associated with one article
    * @ORM\ManyToOne(targetEntity=Article::class,inversedBy="images")
    * @ORM\JoinColumn(name="article_id", referencedColumnName="id_article")
    */
    protected $article;


    function __construct($url_image) {
        $this->url_image = $url_image;

    }

    // Fonctions membres
    //getters
    public function getId()
    {
        return $this->id_image ;
    }

    public function getUrl()
    {
        return $this->url_image ;
    }


    //setters

    public function setUrl($url_image)
    {
        $this->url_image = $url_image;
    }

    public function setArticle($article)
    {
        $this->article = $article;
    }


    public function __toString()
    {
        $format = "Image (id: %s, url: %s)\n";
        return sprintf($format, $this->id_image, $this->url_image);
    }

}