<?php
namespace Site\Entity;
use Doctrine\Common\Collections\ArrayCollection; 

use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
*@ORM\Table(name="personne")
*/

class Personne
{
    // Variables membres

    /**
    * @ORM\Id
    * @ORM\GeneratedValue
    * @ORM\Column(type="integer")
    */
    private $id_personne;

    /**
    * @ORM\Column(type="string")
    */
    private $nom_personne;

    /**
    * @ORM\Column(type="string")
    */
    private $email_personne;

    /**
    * @ORM\Column(type="string")
    */
    private $mdp_personne;

    /**
    * @ORM\Column(type="boolean",options={"default":false},nullable=true)
    */
    private $isMailValidated;

    /**
    * @ORM\Column(type="string",nullable=true)
    */
    private $profil_picture_personne;

    /**
     * One article has many images. This is the inverse side.
     * @ORM\OneToMany(targetEntity=Article::class, cascade={"persist", "remove"}, mappedBy="personne")
     */
    private $articles;

    /**
     * One person can make many purchase. This is the inverse side.
     * @ORM\OneToMany(targetEntity=Commande::class, cascade={"persist", "remove"}, mappedBy="personne")
     */
    private $commandes;

    function __construct($nom,$email,$mdp) {
        $this->nom_personne = $nom;
        $this->email_personne = $email;
        $this->mdp_personne = $mdp;
        $this->profil_picture_personne = $profil_picture_personne;
        $this->articles = new ArrayCollection();
        $this->commandes = new ArrayCollection();
        $this->isMailValidated = false;
    }

    // Fonctions membres
    //getters
    public function getId()
    {
        return $this->id_personne ;
    }

    public function getNom()
    {
        return $this->nom_personne ;
    }

    public function getEmail()
    {
        return $this->email_personne ;
    }

    public function getMdp()
    {
        return $this->mdp_personne;
    }

    public function getProfilePicture()
    {
        return $this->profil_picture_personne;
    }

    public function getArticles()
    {
        return $this->articles;
    }

    public function getCommandes()
    {
        return $this->commandes;
    }

    public function getIsMailValidated()
    {
        return $this->isMailValidated;
    }

    //setters
    public function setNom($nom_personne)
    {
        $this->nom_personne = $nom_personne ;
    }


    public function setEmail($email_personne)
    {
        $this->email_personne = $email_personne;
    }

    public function setMdp($mdp_personne)
    {
        $this->mdp_personne = $mdp_personne;
    }

    public function setIsMailValidated($isMailValidated)
    {
        $this->isMailValidated = $isMailValidated;
    }

    public function setProfilePicture($profil_picture_personne)
    {
        $this->profil_picture_personne = $profil_picture_personne;
    }

    public function addArticle(Article $article)
    {
        $this->articles->add($article);
        $article->setPersonne($this);
    }

    public function addCommande(Commande $commande)
    {
        $this->commandes->add($commande);
        $commande->setPersonne($this);
    }

    public function __toString()
    {
        $format = "Personne (id: %s, nom: %s, email: %s, mdp: %s)\n";
        return sprintf($format, $this->id_personne, $this->nom_personne, $this->email_personne, $this->mdp_personne);
    }

}