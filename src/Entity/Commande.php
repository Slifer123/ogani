<?php
namespace Site\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
*@ORM\Table(name="commandes")
*/

class Commande
{
    // Variables membres

    /**
    * @ORM\Id
    * @ORM\GeneratedValue
    * @ORM\Column(type="integer")
    */
    private $id_commande;

    /**
    * @ORM\Column(type="float")
    */
    private $total_ttc_panier;

    /**
    * @ORM\Column(type="string")
    */
    private $panier;

    /**
    * many purchases can be associated to one person
    * @ORM\ManyToOne(targetEntity=Personne::class,inversedBy="commandes")
    * @ORM\JoinColumn(name="id_personne", referencedColumnName="id_personne")
    */
    protected $personne;


    function __construct($panier,$total_ttc_panier) {
        $this->panier = $panier;
        $this->total_ttc_panier = $total_ttc_panier;

    }

    // Fonctions membres
    //getters
    public function getId()
    {
        return $this->id_commande ;
    }

    public function getTotalTTCPanier()
    {
        return $this->total_ttc_panier;
    }

    public function getPanier()
    {
        return $this->panier;
    }


    //setters

    public function setTotalTTCPanier($total_ttc_panier)
    {
        $this->total_ttc_panier = $total_ttc_panier;
    }

    public function setPanier($panier)
    {
        $this->panier = $panier;
    }

    public function setPersonne($personne)
    {
        $this->personne = $personne;
    }


    public function __toString()
    {
        $format = "Image (id: %s, url: %s)\n";
        return sprintf($format, $this->id_image, $this->url_image);
    }

}