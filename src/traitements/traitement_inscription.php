<?php
//include "../pages/Global/pdo.php";
//include "../../bootstrap.php";
include "../utile/config.php";
session_start();

use Site\Entity\Personne;
use Twilio\Rest\Client;

if(isset($_POST['nom']) && !empty($_POST['nom']) && 
    isset($_POST['email']) && !empty($_POST['email']) &&
    isset($_POST['password']) && !empty($_POST['password'])
    ){
        extract($_POST);
        echo "on entre".$password;
        
        $article_titre = htmlentities($nom);
        $article_description = htmlentities($email);
        $article_categorie = htmlentities($password);
        
        $entityManager = require_once "../../bootstrap.php";

        // Instanciation de l'utilisateur

        $admin = new Personne($nom,$email,$password);
        
        // Gestion de la persistance
        $entityManager->persist($admin);
        $entityManager->flush();
        
        // Vérification du résultats
        echo "Identifiant de l'utilisateur créé : ", $admin->getId();
        //die;
        
        //***********************************Envoie un mail de vérification*******************************
        // validate the email address and password
        // then store in the database for later processing with a not verified flag

        // create a new Twilio Rest Client
        $client = new Client(
            TWILIO_ACCOUNT_SID,TWILIO_AUTH_TOKEN
        
        );

        // send the verification request to the Twilio API
        $verification = $client->verify
        ->v2
        // service id of the verification service we created
        ->services(TWILIO_VERIFY_SERVICE_SID)
        ->verifications
        ->create($email, "email", [
                'channelConfiguration' => [
                    'substitutions' => [
                        'email' => $email,
                        'link' => HOST_NAME.':'.PORT.'/ogani/src/pages/Global/verify_mail.php',
                        //'link' => HOST_NAME_PROD.'/verify_mail.php',
                    ]
                ],
            ]
        );

        // store the email address in the session for use when the verify link is clicked
        $_SESSION['email_address_verify'] = $email;





        header('Location: ../pages/Global/connexion.php?isRegistered=true');

        print_r($_POST);
        echo '<div class="alert alert-success" role="alert">';
            echo 'Message envoyé';
        echo '</div>';
       
    }
