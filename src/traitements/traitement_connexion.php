<?php
include "../pages/Global/pdo.php";
use Site\Entity\Personne;
$entityManager = require_once "../../bootstrap.php";




if( isset($_POST['email']) && !empty($_POST['email']) &&
    isset($_POST['password']) && !empty($_POST['password'])
    ){
        
        extract($_POST);
        
        $article_titre = htmlentities($nom);
        $article_description = htmlentities($email);
        $article_categorie = htmlentities($password);
        
       
        $personneRepo = $entityManager->getRepository(Personne::class);

        $personneByEmailAndPassword = $personneRepo->findBy(["email_personne" => $email, 
                                                              "mdp_personne" => $password
                                                            ]);
        //var_dump($email);
        //Si l'email existe dans la bdd
        if($personneByEmailAndPassword != null)
        {
            print_r($personneByEmailAndPassword[0]->getId());

            //Si le user a validé son email
            if($personneByEmailAndPassword[0]->getIsMailValidated()==true)
            {
                header('Location: ../pages/Global/index.php?isConnected=true&id='.$personneByEmailAndPassword[0]->getId());
            }
            else
            {
                header('Location: ../pages/Global/connexion.php?isEmailNonVerified=true');
            }
            
            
        }
        //Si l'email n'existe pas
        else
        {
            header('Location: ../pages/Global/connexion.php?isEmailNonCreated=true');
        }

        
       
    }