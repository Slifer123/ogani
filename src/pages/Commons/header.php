<?php include "../../utile/formatage.php";
      include "../../utile/requetage.php";
      include "../../utile/config.php" ;
    //API KEY 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
     //echo phpinfo();
     //die;
     use Site\Entity\Personne;
     use Site\Entity\Article;
     use Site\Entity\Image;
     
     session_start();
     //echo "ici".$_SESSION['mail'];
     $entityManager = require_once "../../../bootstrap.php";
     
     $personneRepo = $entityManager->getRepository(Personne::class);
     
     //       //session_start();
     //       //echo $_SESSION['nom'];
     //       //print_r($entityManager);
     
     //Lorsqu'on se connecte on renvoie ici l'id du user connecté pour creer les variables de sessions
        if( isset($_GET['id']) && !empty($_GET['id']))
        {
            extract($_GET);
            
            $id = htmlentities($id);
            
            $personneRepo = $entityManager->getRepository(Personne::class);

            $personneById = $personneRepo->findBy(["id_personne" => $id
                                                                ]);


            if($personneById != null)
            {
                //print_r($personneById[0]);

                session_start();
                $_SESSION['nom'] = $personneById[0]->getNom();
                $_SESSION['mail'] = $personneById[0]->getEmail();
                $_SESSION['mdp'] = $personneById[0]->getMdp();
                $_SESSION['personne'] = $personneById[0];
                
                //header('Location: ../pages/Global/index.php?isConnected=true&id='.$personneByEmailAndPassword[0]->getId());
            }

                
            
        }

     //Suppression article du panier
     if (isset($_POST['action']) && $_POST['action']=="remove")
     {
        //echo "on entre ici".$_POST["id"];
        //var_dump($_POST);
        
       if(!empty($_SESSION["shopping_cart"])) 
       {
         //var_dump($_SESSION["shopping_cart"] );
         //die();
           foreach($_SESSION["shopping_cart"] as $key => $value) 
           {
           // var_dump($value);
            //die();
             if($_POST["id"] == $value['id']){
                 print_r($_SESSION["shopping_cart"]);
             unset($_SESSION["shopping_cart"][$key]);
            
             //die();
             $status = "<div class='box' style='color:red;'>
                 Ce produit a été retiré de votre panier</div>";
             }
             if(empty($_SESSION["shopping_cart"]))
             unset($_SESSION["shopping_cart"]);
           }		
       }
     }
     
     if(isset($_GET['idArticle']))
     {
         //var_dump($_GET['idArticle']);
         $query = getAllAboutSpecificArticle($entityManager,$_GET['idArticle']);
     
         
         //var_dump($query);
     }
     
     
     //Partie selection d'un article
     if(isset($_POST['code']) && $_POST['code']== "ajouterArticle" && $_GET['idArticle'])
     {
         //echo "on entre";
     
         $queryArticle = getAllAboutAnArticle($entityManager,$_GET['idArticle']);
     
         foreach ($queryArticle->getResult() as $article) {
             //echo $article->getId()."\n";
             $id = $article->getId();
             $cartArray = array(
                 $id=>array(
                 'id'=>$id,
                 'titre'=>$article->getTitre(),
                 'prixVente'=>$article->getPrixVente(),
                 'quantity'=>1,
                 'image'=>$article->getImages()->first()->getUrl() )
                );
     
             if(empty($_SESSION["shopping_cart"])) {
                 $_SESSION["shopping_cart"] = $cartArray;
                 $status = "<div class='box' style='color:red;'>Produit ajouté au panier</div>";
             }
             else
             {
                 
                 $array_keys = array_keys($_SESSION["shopping_cart"]);
                 
                 
                 if(in_array($id,$array_keys)) 
                 {
                     $status = "<div class='box' style='color:red;'>
                                     Produit déjà ajouté
                                 </div>"; 
                                
                 } 
                 else 
                 {
                     
                 
                 $_SESSION["shopping_cart"] = $_SESSION["shopping_cart"]+$cartArray;
                 //print_r($_SESSION["shopping_cart"]);
                 $status = "<div class='box' style='color:red;'>Produit ajouté au panier</div>";
              }
             }
             
             
         }
     
     
     
     }

     //Si le user finalise la commande, on détruit la variable de session du contenant les articles
     if(isset($_GET['isCommandTerminated']) && $_GET['isCommandTerminated']="true")
    {
        unset($_SESSION['shopping_cart']);
    }
     
      
      
      
      
?>
<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Ogani Template">
    <meta name="keywords" content="Ogani, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ogani | Template</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    
    
    
    <link rel="stylesheet" href="../../bootstrap/css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="../../bootstrap-select/dist/css/bootstrap-select.css">
    
    
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/css/fileinput-rtl.min.css" media="all" rel="stylesheet" type="text/css" />
    
    
    <link rel="stylesheet" href="../../css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="../../css/elegant-icons.css" type="text/css">
    
    <link rel="stylesheet" href="../../css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="../../css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="../../css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="../../css/style.css" type="text/css">
    <link rel="stylesheet" href="../../css/main.css" type="text/css">
    

    <link href='../../dropzone/dist/dropzone.css' type='text/css' rel='stylesheet'>
    <script src='../../dropzone/dist/dropzone.js' type='text/javascript'></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?version=3.52.1&features=fetch"></script>
    <script src="https://js.stripe.com/v3/"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Humberger Begin -->
    <div class="humberger__menu__overlay"></div>
    <div class="humberger__menu__wrapper">
        <div class="humberger__menu__logo">
            <!--<a href="#"><img src="img/logo.png" alt=""></a>-->
        </div>
        <div class="humberger__menu__cart">
            <ul>
                <li><a href="shoping-cart.php"><i class="fa fa-shopping-bag"></i> <span>
                                                                                                        <?php
                                                                                                            if(!empty($_SESSION["shopping_cart"])) {
                                                                                                                $cart_count = count(array_keys($_SESSION["shopping_cart"])); 
                                                                                                                echo $cart_count; 
                                                                                                            }
                                                                                                            else{
                                                                                                                echo 0;              
                                                                                                            }
                                                                                                            
                                                                                                        ?>
                                                                    </span></a></li>
            </ul>
            
        </div>
        <div class="humberger__menu__widget">
            <div id ="" class="header__top__right__auth iconeUserNotConnected">
                <a href="../Global/connexion.php"><i class="fa fa-user"></i> Se connecter | S'inscrire</a>
            </div>
            <div id ="" class="header__top__right__auth iconeUserConnected">
                <a class="dropdown-toggle " href="#" id="dropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i> <?php echo $_SESSION['nom'];?></a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                    <a class="dropdown-item" href="../Global/deconnexion.php">Déconnexion</a>
                                    <a class="dropdown-item" href="../Global/monprofil.php">Mon profil</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                </div>
            </div>
        </div>
        <nav class="humberger__menu__nav mobile-menu">
            <ul>
                <li><a href="../Global/index.php">Accueil</a></li>
                <li><a href="../Global/shop-grid.php">Boutique</a></li>
                <li><a href="../Global/ajouter_article.php">Ajouter un article</a></li>
            </ul>
        </nav>
        <div id="mobile-menu-wrap"></div>
        <div class="header__top__right__social">
            <a href="#"><i class="fab fa-facebook"></i></a>
            <a href="#"><i class="fab fa-twitter"></i></a>
            <a href="#"><i class="fab fa-linkedin"></i></a>
            <a href="#"><i class="fab fa-pinterest-p"></i></a>
        </div>
    </div>
    <!-- Humberger End -->
    

    <!-- Header Section Begin -->
    <header class="header">
        <div class="header__top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        
                    </div>
                    <div class="col-lg-6">
                        <div class="header__top__right">
                            <div class="header__top__right__social">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-linkedin"></i></a>
                                <a href="#"><i class="fa fa-pinterest-p"></i></a>
                            </div>
                            
                            <div id ="" class="header__top__right__auth iconeUserNotConnected headerStyle">
                                <a href="../Global/connexion.php"><i class="fa fa-user"></i> Se connecter | S'inscrire</a>
                            </div>

                            <div id ="" class="header__top__right__auth iconeUserConnected">
                                <a class="dropdown-toggle " href="#" id="dropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i> 
                                    <?php echo $_SESSION['nom'];?>
                                </a>
                                
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                    <a class="dropdown-item" href="../Global/deconnexion.php">Déconnexion</a>
                                    <a class="dropdown-item" href="../Global/monprofil.php">Mon profil</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="header__logo">
                        <a href="../Global/index.php"><img src="../../img/logo.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <nav class="header__menu">
                        <ul>
                            <li><a class="headerStyle" href="../Global/index.php">Accueil</a></li>
                            <li><a class="headerStyle" href="../Global/shop-grid.php">Boutique</a></li>
                            <li><a class="headerStyle" href="../Global/ajouter_article.php">Ajouter un article</a></li>
                            
                            
                            <!--<li><a href="../Global/contact.php">Contact</a></li>-->
                        </ul>
                    </nav>
                </div>
                <div class="col-lg-3">
                    
                        <div class="header__cart">
                            <ul>
                            <!-- <li><a href="#"><i class="fa fa-heart"></i> <span>1</span></a></li>-->
                                <li><a href="shoping-cart.php"><i class="fa fa-shopping-bag"></i> <span><?php
                                                                                                            if(!empty($_SESSION["shopping_cart"])) {
                                                                                                                $cart_count = count(array_keys($_SESSION["shopping_cart"])); 
                                                                                                                echo $cart_count; 
                                                                                                            }
                                                                                                            else{
                                                                                                                echo 0;              
                                                                                                            }
                                                                                                            
                                                                                                        ?>
                                                                                                            
                                                                                                    </span>
                                    </a>
                                </li>
                            </ul>
                            <!--<div class="header__cart__price">item: <span>$150.00</span></div>-->
                        </div>
                   
                </div>
            </div>
            <div class="humberger__open">
                <i class="fa fa-bars"></i>
            </div>
        </div>
    </header>

    <?php
    
   
    
    
    
    
    ?>
    
    <!-- Header Section End -->
