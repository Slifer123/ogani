</div>

        <!--Footer du site-->
        <!-- Footer Section Begin -->
    <footer class="footer spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="footer__about">
                        <div class="footer__about__logo">
                            <a href="./index.html"><img src="../../img/logo.png" alt=""></a>
                        </div>
                        <ul>
                            <li>Address: 60-49 Road 11378 New York</li>
                            <li>Phone: +65 11.188.888</li>
                            <li>Email: hello@colorlib.com</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 offset-lg-1">
                    <div class="footer__widget">
                        <h6>Useful Links</h6>
                        <ul>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">About Our Shop</a></li>
                            <li><a href="#">Secure Shopping</a></li>
                            <li><a href="#">Delivery infomation</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Our Sitemap</a></li>
                        </ul>
                        <ul>
                            <li><a href="#">Who We Are</a></li>
                            <li><a href="#">Our Services</a></li>
                            <li><a href="#">Projects</a></li>
                            <li><a href="#">Contact</a></li>
                            <li><a href="#">Innovation</a></li>
                            <li><a href="#">Testimonials</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="footer__widget">
                        <h6>Join Our Newsletter Now</h6>
                        <p>Get E-mail updates about our latest shop and special offers.</p>
                        <form action="#">
                            <input type="text" placeholder="Enter your mail">
                            <button type="submit" class="site-btn">Subscribe</button>
                        </form>
                        <div class="footer__widget__social">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-instagram"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-pinterest"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer__copyright">
                        <div class="footer__copyright__text"><p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p></div>
                        <div class="footer__copyright__payment"><img src="../../img/payment-item.png" alt=""></div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    
    <script src = "../../js/functions.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    
    
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    
    
    
    <script src="../../js/jquery-3.3.1.min.js"></script>
    <script src="../../js/bootstrap.min.js"></script>
    
    <script src="../../js/jquery-ui.min.js"></script>
    <script src="../../js/jquery.slicknav.js"></script>
    <script src="../../js/mixitup.min.js"></script>
    <script src="../../js/owl.carousel.min.js"></script>
    <script src="../../js/main.js"></script>
    

    

    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
    <script src="../../bootstrap/js/bootstrap.js"></script>
   
    <script type="text/javascript" src="../../bootstrap-select/dist/js/bootstrap-select.min.js"></script>

    <script src="https://www.paypal.com/sdk/js?&client-id=AZno09yCJ5viGmMx1VbYfwDWzVq6OACFYHVuMxMdcqMGwNo4bgzs-4g2NK9vSW1Duy2Ugj_VBrXaKtVF&merchant-id=<Merchant-ID>"></script>

    
    

    <script>
        //Select picker pour le select de l'ajout d'un d'article
        $(document).ready(function () {
            
                $('.selectpicker').selectpicker();
                
                console.log("true9");
            
            console.log("true1");

           
        });

        

        $(document).ready(function() {
            //$("#success-alert").hide();
            $(".iconeUserConnected").hide();
            $("#success-alert").hide();
            $("#registered-alert").hide();
            $("#articleAddedSuccess-alert").hide();
            $("#logonRequired-alert").hide();
            $("#mailVerified-alert").hide();
            $("#mailCodeNonValid-alert").hide();
            $("#mailNonVerified-alert").hide();
            $("#mailNonCreated-alert").hide();
            $("#isCommandTerminatedSuccess-alert").hide();
            
            
            var sessionId = "<?php echo $_SESSION['nom']; ?>";
            if ($_GET('isConnected') == "true")
            {   
                console.log("true1");
                $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
                    $("#success-alert").slideUp(500);
                });

                $(".iconeUserNotConnected").hide();
                $(".iconeUserConnected").show();
            }
            else if (sessionId)//Si on est connecté
            {   
                console.log("true2");
                console.log(sessionId);

                $(".iconeUserNotConnected").hide();
                $(".iconeUserConnected").show();

                if ($_GET('isArticleAdded') == "true")
                {   
                    console.log("tru5");
                    $("#articleAddedSuccess-alert").fadeTo(2000, 500).slideUp(500, function(){
                        $("#articleAddedSuccess-alert").slideUp(500);
                    });

                
                }
                //Si la commande est validée 
                else if ($_GET('isCommandTerminated') == "true")
                {   
                    console.log("ici c'est paris");
                    $("#isCommandTerminatedSuccess-alert").fadeTo(2000, 500).slideUp(1000, function(){
                        $("#isCommandTerminatedSuccess-alert").slideUp(500);
                    });

                
                }
            }
            else if ($_GET('isRegistered') == "true")
            {   
                console.log("tru4");
                $("#registered-alert").fadeTo(2000, 500).slideUp(500, function(){
                    $("#registered-alert").slideUp(500);
                });

            
            }
            else if ($_GET('isMailVerified') == "true")
            {   
                console.log("tru4");
                $("#mailVerified-alert").fadeTo(2000, 500).slideUp(500, function(){
                    $("#mailVerified-alert").slideUp(500);
                });

            
            }
            else if ($_GET('isMailCodeNotValid') == "true")
            {   
                console.log("tru4");
                $("#mailCodeNonValid-alert").fadeTo(2000, 500).slideUp(500, function(){
                    $("#mailCodeNonValid-alert").slideUp(500);
                });

            
            }
            else if ($_GET('isEmailNonVerified') == "true")
            {   
                console.log("tru4");
                $("#mailNonVerified-alert").fadeTo(2000, 500).slideUp(500, function(){
                    $("#mailNonVerified-alert").slideUp(500);
                });

            
            }
            else if ($_GET('isEmailNonCreated') == "true")
            {   
                console.log("tru4");
                $("#mailNonCreated-alert").fadeTo(2000, 500).slideUp(500, function(){
                    $("#mailNonCreated-alert").slideUp(500);
                });

            
            }
            
            else if ($_GET('isArticleAdded') == "true")
            {   
                console.log("tru4");
                $("#articleAddedSuccess-alert").fadeTo(2000, 500).slideUp(500, function(){
                    $("#articleAddedSuccess-alert").slideUp(500);
                });

            
            }
            else if ($_GET('logonRequired') == "true")
            {   
                console.log("tru4");
                $("#logonRequired-alert").fadeTo(2000, 500).slideUp(1000, function(){
                    $("#logonRequired-alert").slideUp(500);
                });

            
            }
            
            console.log($_GET('isCommandTerminated'));
            
        });

        //Changer l'image de profil de l'utilisateur
        $("#profileImage").click(function(e) {
            $("#imageUpload").click();
        });

        function fasterPreview( uploader ) {
            if ( uploader.files && uploader.files[0] ){
                $('#profileImage').attr('src', 
                    window.URL.createObjectURL(uploader.files[0]) );
            }
        }

        $("#imageUpload").change(function(){
            fasterPreview( this );
            $("#uploadProfileImage").click();
        });

       /* $(".gallery").magnificPopup({
            delegate: 'a',
            type: 'image',
            gallery:{
                enabled: true
            }
            });*/

            function myFunction(smallImg)
            {
                console.log("on est entré")
                var fullImg = document.getElementById("imageBox");
                fullImg.src = smallImg.src;
                console.log(smallImg.src)
            }

            $(document).ready(function () {
            
                var session = "<?php echo $_SESSION["shopping_cart"]; ?>";

                <?php $_SESSION["shopping_cart"] = $_SESSION["shopping_cart"]; ?>
            
                console.log(session);
            });

            $(".logo-slider").slick({
                slidesToShow: 3,
                dots:true,
                centerMode: true,
                    autoplay:true
            });


            $(document).ready(function () {
            
                // Get the modal
                var modal = document.querySelector("#myModal");

                // Get the image and insert it inside the modal - use its "alt" text as a caption
                //var img = document.querySelector("myImg");
                var modalImg = document.querySelector(".modal-content");
                //var captionText = document.querySelector("caption");
                Array.from(document.querySelectorAll("#myImg")).forEach(item => {
                    item.addEventListener("click", event => {
                    modal.style.display = "block";
                    modalImg.src = event.target.src;
                    });
                });
                document.querySelector(".close").addEventListener("click", () => {
                modal.style.display = "none";
                });

                
            });

            
        

        
                    
    </script>
    
    
</body>
<?php 
//       session_start();
//       $shopping = $_SESSION["shopping_cart"];  
//       $_SESSION["cart"]=$_SESSION["shopping_cart"]; 
// print_r($_SESSION["cart"]);?>
</html>