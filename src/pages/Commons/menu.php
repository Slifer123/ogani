<!--<nav class="navbar navbar-expand-lg navbar-dark bg-dark perso_fontSizeMenu">
  
 

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto mx-auto">

        <li class="nav-item">
          <a class="nav-link" href="#">Link</a>
        </li>

        <form class="form-inline my-2 my-lg-0">
          <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        
        </form>

      

      
      
    </ul>
    
  </div>
</nav>-->

<nav class="navbar navbar-expand-lg navbar-dark  align-items-center bg-dark">
  
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  

  <div class="navbar collapse navbar-collapse align-items-center  " id="navbarSupportedContent">
    <div class="d-flex flex-row">
      <ul class="navbar-nav mr-auto align-items-center">
        <li class="nav-item active">
          <a class="ajouter " href="../Global/ajouter_article.php">
            <i class="fas fa-plus-square pr-2" style="font-size:20px;"></i>
            <span>Ajouter un article</span>
          </a>
        </li>
        
      </ul>
      <form class="form-inline  ml-5">
        <input class="form-control mr-sm-2" type="search" placeholder="Recherchez" aria-label="Search" size="50">
        <input type="submit" style="display: none;" />
        
      </form>
    </div>
  </div>

</nav>