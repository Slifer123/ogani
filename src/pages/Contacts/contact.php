<?php include "../Commons/header.php";?>

    <?= formatTitrePageH1("Contact", COLOR_ASSO) ?>

    <?= formatTitrePageH3("Suivez-nous :", "");?>
    <p class="pl-5">
        <a href="https://www.facebook.com/nosamisnosanimaux/" target=_blank><img src="../../sources/images/Autres/icones/facebook.png" width="30px" alt="Facebook"/></a>
        Suivez-nous sur facebook et participez à l'aventure des Kings: 
        <a href="https://www.facebook.com/nosamisnosanimaux/" target=_blank><span class="badge badge-pill badge-primary">Notre facebook</span></a>
    </p>

    <?= formatTitrePageH3("Contactez-nous :", "") ?>
    <div class="pl-5">
        <p>
            <img src="../../sources/images/Autres/icones/courrier.png" width="30px" alt="courrier"/>
            Par courrier : Hameau de la Souleille - 09420 Clermont, Midi-Pyrenees, France
        </p>
        <p>
            <img src="../../sources/images/Autres/icones/mail.png" width="30px" alt="mail"/>
            Par mail : <a href="mailto:king@gmail.com">kings@gmail.com</a>
        </p>
        <p>
            <img src="../../sources/images/Autres/icones/tel.png" width="30px" alt="tel"/>
            Par téléphone : 06 10 59 94 71
        </p>
        <p>
            <span class="badge badge-pill badge-danger">Ou</span> par notre <span class="badge badge-pill badge-warning">formulaire</span> de contact :
        </p>
    </div>

    <?= formatTitrePageH3("Formulaire de contact :", "") ?>
    <?php 
    if(isset($_POST['nom']) && !empty($_POST['nom']) && 
    isset($_POST['mail']) && !empty($_POST['mail']) &&
    isset($_POST['objet']) && !empty($_POST['objet']) &&
    isset($_POST['message']) && !empty($_POST['message']) &&
    isset($_POST['captcha']) && !empty($_POST['captcha'])
    ){
        $captcha = (int) $_POST['captcha'];
        if($captcha === 8){
            $nom = htmlentities($_POST['nom']);
            $mail = htmlentities($_POST['mail']);
            $objet = htmlentities($_POST['objet']);
            $message = htmlentities($_POST['message']);
            $destinataire = "kings@gmail.com";
            mail($destinataire, $objet. " - " . $nom, "Mail : ". $mail. " Message : " . $message);
            echo '<div class="alert alert-success" role="alert">';
                echo 'Message envoyé';
            echo '</div>';
        } else {
            echo '<div class="alert alert-danger" role="alert">';
            echo 'Erreur de Captcha, recommencer';
            echo '</div>';
        }
    }
    ?>
    <form method="POST" action="#" class="pl-5">
        <div class="form-group row no-gutters align-items-center">
            <label for="nom" class="col-auto pr-3">Nom :</label>
            <input type="text" name="nom" id="nom" class="form-control col" Placeholder = "nom" required/>
        </div>
        <div class="form-group row no-gutters align-items-center">
            <label for="mail" class="col-auto pr-3">Email :</label>
            <input type="email" name="mail" id="mail" class="form-control col" Placeholder = "nom@exemple.com" required/>
        </div>
        <div class="form-group row no-gutters align-items-center">
            <label for="objet" class="col-auto pr-3">Objet :</label>
            <select class='form-control col' id="objet" name="objet">
                <option value="question">Question</option>
                <option value="adoption">Adoption</option>
                <option value="donnation">Donnation</option>
                <option value="autre">Autre</option>
            </select>
        </div>
        <div class="form-group">
            <label for="message">Message :</label>
            <textarea class="form-control" id="message" name="message" rows="3" required></textarea>
        </div>
        <div class="form-group row no-gutters align-items-center">
            <label for="captcha" class="col-auto pr-3">Combien font 3+5 :</label>
            <input type="number" name="captcha" id="captcha" class="form-control col" required/>
        </div>
        <input type="submit" class="btn btn-primary mx-auto d-block" value="Valider" />
    </form>




<?php include "../Commons/footer.php";?>