<?php 

include "../Commons/header.php";
session_start();

use Twilio\Rest\Client;
use SendGrid\Mail\From;
use SendGrid\Mail\HtmlContent;
use SendGrid\Mail\Mail;
use SendGrid\Mail\PlainTextContent;
use SendGrid\Mail\To;

$email = 'stevezanou@gmail.com';
$password = 'toto';
$link = $_SERVER['HTTP_HOST'] . str_replace("handle", "verify_mail", $_SERVER['REQUEST_URI']);
// validate the email address and password
// then store in the database for later processing with a not verified flag

// create a new Twilio Rest Client
$client = new Client(
   TWILIO_ACCOUNT_SID,
   TWILIO_AUTH_TOKEN
);

// send the verification request to the Twilio API
$verification = $client->verify
   ->v2
   // service id of the verification service we created
   ->services("VA8ee4ff6dc6fcaef38df028b8d04b97ab")
   ->verifications
   ->create($email, "email", [
           'channelConfiguration' => [
               'substitutions' => [
                   'email_address' => $email,
                   'email' => $email,
                   'link' => HOST_NAME.':'.PORT.'/ogani/src/pages/Global/verify_mail.php',
                   //'link' => HOST_NAME_PROD.'/verify_mail.php',
               ]
           ],
       ]
   );

// store the email address in the session for use when the verify link is clicked
$_SESSION['email_address_verify'] = $email;
//echo "http://" . $_SERVER['HTTP_HOST'] . str_replace("handle", "verify_mail", $_SERVER['REQUEST_URI']);



/*$from = new From("steven.zanou@groupe-esigelec.org", "Buyandsell");
$tos = [
    new To(
        "stevezanou@gmail.com",
        "Buyandsell",
        [
            'link' => 'verify_mail.php',
            
        ],
        "Vérifiez votre adresse mail"
    )
];
$email = new Mail(
    $from,
    $tos
);
$email->setTemplateId(SENDGRID_TEMPLATE_ID);
$sendgrid = new \SendGrid(SENDGRID_API_KEY);
try {
    $response = $sendgrid->send($email);
    print $response->statusCode() . "\n";
    print_r($response->headers());
    print $response->body() . "\n";
} catch (Exception $e) {
    echo 'Caught exception: '.  $e->getMessage(). "\n";
}*/




?>