<section class="hero">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="hero__categories">
                        <div class="hero__categories__all">
                            <i class="fa fa-bars"></i>
                            <span>Catégories</span>
                        </div>
                        <ul>
                            <li><a href="shop-grid.php?categorie=informatique">Informatique</a></li>
                            <li><a href="shop-grid.php?categorie=console_jeu">Console & Jeux vidéo</a></li>
                            <li><a href="shop-grid.php?categorie=image_son">Image & Son</a></li>
                            <li><a href="shop-grid.php?categorie=telephonie">Téléphonie</a></li>
                            <li><a href="shop-grid.php?categorie=vetement">Vêtements</a></li>
                            <li><a href="shop-grid.php?categorie=chaussure">Chaussures</a></li>
                            <li><a href="shop-grid.php?categorie=accessoire_bagagerie">Accessoires et Bagagerie</a></li>
                            <li><a href="shop-grid.php?categorie=montre_bijou">Montres & Bijoux</a></li>
                            <li><a href="shop-grid.php?categorie=equipement_bebe">Equipements bébé</a></li>
                            <li><a href="shop-grid.php?categorie=vetement_bebe">Vêtements bébé</a></li>
                            <li><a href="shop-grid.php?categorie=luxe_tendance">Luxe & Tendances</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="hero__search">
                        <div class="hero__search__form">
                            <form method="post" action="shop-grid.php">
                                <input type="text" name="recherche" placeholder="Recherche">
                                <button type="submit" class="site-btn"><i class="fas fa-search"></i></button>
                            </form>
                        </div>
                       
                    </div>
                    <div class="hero__item set-bg" data-setbg="../../img/hero/banner2.png">
                        <div class="hero__text">
                            
                            <h2>Plus de place ? <br />Revendez <br /></h2>
                            
                            <a href="start.php" class="primary-btn">Commencer</a>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>