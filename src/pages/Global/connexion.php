<?php include "../Commons/header.php";?>



<div class="alert alert-success" id="registered-alert">
  <button type="button" class="close" data-dismiss="alert">x</button>
  <strong>Veuillez confirmer votre email dans votre boîte de réception </strong>
</div>

<div class="alert alert-success" id="mailVerified-alert">
  <button type="button" class="close" data-dismiss="alert">x</button>
  <strong>Vous pouvez maintenant vous connecter</strong>
</div>

<div class="alert alert-danger" id="mailCodeNonValid-alert">
  <button type="button" class="close" data-dismiss="alert">x</button>
  <strong>Votre code n'est pas valide ou a expiré</strong>
</div>

<div class="alert alert-danger" id="mailNonVerified-alert">
  <button type="button" class="close" data-dismiss="alert">x</button>
  <strong>Veuillez confirmer votre email avant de vous connecter. Vérifiez votre boîte aux lettres</strong>
</div>

<div class="alert alert-danger" id="mailNonCreated-alert">
  <button type="button" class="close" data-dismiss="alert">x</button>
  <strong>Oups! Nous n'avons pas reconnu votre email</strong>
</div>

<div class="alert alert-danger" id="logonRequired-alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <strong>Connexion requise avant de pouvoir valider votre commande </strong>
</div>


    <div class=" align-items-center center">
        <?= formatTitrePageH3("Se connecter", "") ?>
        <div class="col-md-4 offset-md-4">
            <form method="POST" action="../../traitements/traitement_connexion.php" class="mt-3 pt-3 pl-3 pr-3 rounded perso_shadow">
                
               

                <div class="form-group row no-gutters align-items-center">
                    
                    <input type="email" name="email" id="email" class="form-control " Placeholder = "Email" required/>
                </div>

                <div class="form-group row no-gutters align-items-center">
                    
                    <input type="password" name="password" id="password" class="form-control " Placeholder = "Mot de passe" required/>
                </div>

                <a href="reset_password.php" class="seconnecter">
                    <p>
                        Mot de passe oublé
                    </p>
                </a>

                
                <input type="submit" class="btn btn-primary mx-auto d-block" value="Valider" />

                
                    <p class="text-center mt-4">
                        Pas de compte ? <a href="inscription.php" class="seconnecter">S'inscrire</a>
                    </p>
                

            </form>
        </div>
    </div>




<?php include "../Commons/footer.php";?>