<?php include "../Commons/header.php";

use Site\Entity\Personne;
use Site\Entity\Article;
use Site\Entity\Image;
session_start();
//echo "ici".$_SESSION['mail'];
//$entityManager = require_once "../../../bootstrap.php";


$personneRepo = $entityManager->getRepository(Personne::class);

$personneByEmailAndPassword = getPersonneByEmailAndPassword($personneRepo,$_SESSION['mail'],$_SESSION['mdp']);

$status="";
//var_dump($_SESSION["shopping_cart"]);

/*if (isset($_POST['action']) && $_POST['action']=="remove"){

    echo "on entre ici".$_POST["id"];
    var_dump($_POST);
    
   if(!empty($_SESSION["shopping_cart"])) {
     //var_dump($_SESSION["shopping_cart"] );
     //die();
       foreach($_SESSION["shopping_cart"] as $key => $value) {
       // var_dump($value);
        //die();
         if($_POST["id"] == $value['id']){
             print_r($_SESSION["shopping_cart"]);
         unset($_SESSION["shopping_cart"][$key]);
        
         //die();
         $status = "<div class='box' style='color:red;'>
             Ce produit a été retiré de votre panier</div>";
         }
         if(empty($_SESSION["shopping_cart"]))
         unset($_SESSION["shopping_cart"]);
     }		
   }
}*/


?>



    <!-- Hero Section Begin -->
    <!-- Hero Section Begin -->
    <section class="hero hero-normal">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="hero__categories">
                        <div class="hero__categories__all">
                            <i class="fa fa-bars"></i>
                            <span>Catégories</span>
                        </div>
                        <ul>
                            <li><a href="#">Informatique</a></li>
                            <li><a href="#">Console & Jeux vidéo</a></li>
                            <li><a href="#">Image & Son</a></li>
                            <li><a href="#">Téléphonie</a></li>
                            <li><a href="#">Vêtements</a></li>
                            <li><a href="#">Chaussures</a></li>
                            <li><a href="#">Accessoires et Bagagerie</a></li>
                            <li><a href="#">Montres & Bijoux</a></li>
                            <li><a href="#">Equipements bébé</a></li>
                            <li><a href="#">Vêtements bébé</a></li>
                            <li><a href="#">Luxe & Tendances</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="hero__search">
                        <div class="hero__search__form">
                            <form method="post" action="shop-grid.php">
                                <input type="text" name="recherche" placeholder="Recherche">
                                <button type="submit" class="site-btn"><i class="fas fa-search"></i></button>
                            </form>
                        </div>
                       
                    </div>
    
                </div>
            </div>
        </div>
    </section>
    <!-- Hero Section End -->
    <!-- Hero Section End -->

    <!-- Checkout Section Begin -->
    <section class="checkout spad">
        <div class="container">
            
            <div class="checkout__form">
                <h4>Details de la livraison</h4>
                <form target="_self" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">

                    <input type="hidden" name="cmd" value="_cart">
                    <input type="hidden" name="upload" value="1">
                    <!-- Replace "business" value with your PayPal Email Address or Account ID -->
                    <input type="hidden" name="business" value="sb-rfcbs5214405@business.example.com">

                    <?php 
                        $i = 1;
                        foreach ($_SESSION["shopping_cart"] as $product){

                            $url = explode( '../upload/', $product["image"]);
                            //var_dump($product["titre"]);

                    ?>  
                        <!-- Specify details about the item that buyers will purchase. -->
                        
                        <input type="hidden" name=<?php echo "item_name_".$i; ?> value=<?php echo $product["titre"]; ?>>
                        <input type="hidden" name=<?php echo "amount_".$i; ?> value=<?php echo $product["prixVente"]; ?>>
                        <input type="hidden" name=<?php echo "quantity_".$i; ?> value=<?php echo $product["quantity"]; ?>>                  
                    <?php 
                        $i = $i +1;
                                    
                        }
                    ?>

                    <input type="hidden" name="currency_code" value="EUR">
                    <INPUT TYPE="hidden" name="address_override" value="1">
                    <!-- Replace value with the web page you want the customer to return to after a successful transaction -->
                    <input type="hidden" name="return" value=<?php echo HOST_NAME_PORT."/ogani/src/traitements/traitement_commande.php?isCommandValidated=true";?>>
                   <!--  <input type="hidden" name="return" value=<?php echo HOST_NAME_PROD_URL."/src/traitements/traitement_commande.php?isCommandValidated=true";?>>-->
                    <!-- Replace value with the web page you want the customer to return to after item cancellation -->
                    <input type="hidden" name="cancel_return" value="http://www.yourwebsite.com/Cancel.html">
                    <!-- Note: shipping override variable is used with this example -->
                    <input type="hidden" name="shipping" value="20.00">
                    <input type="hidden" name="button_subtype" value="products">
                    <input type="hidden" name="no_note" value="0">
                    <input type="hidden" name="cn" value="Add special instructions to the seller:">
                    <input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynow_LG.gif:NonHosted">

                    <div class="row">
                        <div class="col-lg-8 col-md-6">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Nom<span>*</span></p>
                                        <input type="text" name="first_name" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Prénom<span>*</span></p>
                                        <input type="text" name="last_name">
                                    </div>
                                </div>
                            </div>
                            <div class="checkout__input">
                                <p>Adresse<span>*</span></p>
                                <input type="text" name="address1" placeholder="Voie, Rue,..." class="checkout__input__add">
                                
                            </div>
                            <div class="checkout__input">
                                <p>Ville<span>*</span></p>
                                <input type="text" name="city">
                            </div>
                            
                            <div class="checkout__input">
                                <p>Code Postal<span>*</span></p>
                                <input type="text" name="zip">
                            </div>

                            
                                <input type="hidden" name="country" value="FR">
                            
                            
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="checkout__order">
                                <h4>Votre commande</h4>
                                <div class="checkout__order__products">Produit <span>Total</span></div>
                                <?php 
                                    $total_price = 0;
                                    foreach ($_SESSION["shopping_cart"] as $product){

                                        $url = explode( '../upload/', $product["image"]);
                                        $total_price += ($product["prixVente"]*$product["quantity"]);
                                ?>
                                <ul>
                                    <li><?php echo $product["quantity"]." x ".$product["titre"]; ?><span><?php echo $product["prixVente"]*$product["quantity"]." €"; ?></span></li>
                                </ul>
                                
                                <?php 

                                    
                                    }
                                ?>
                                <div class="checkout__order__subtotal">Sous Total <span><?php echo $total_price." €"; ?></span></div>
                                <div class="checkout__order__total">Total TTC<span><?php echo $_SESSION['total_panier']." €"; ?></span></div>
            
                                <!--<button type="submit" id="pay" class="site-btn">PAIEMENT</button>-->
                                
                                <input type="image" src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/PP_logo_h_100x26.png" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                                <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                            </div>
                        </div>
                    </div>
                    
                </form>
                <!--<button type="submit" id="pay" class="site-btn">PAIEMENT</button>-->
            </div>
        </div>
    </section>
    <!-- Checkout Section End -->

    
    
<script src="https://www.paypal.com/sdk/js?&client-id=AZno09yCJ5viGmMx1VbYfwDWzVq6OACFYHVuMxMdcqMGwNo4bgzs-4g2NK9vSW1Duy2Ugj_VBrXaKtVF&currency=EUR"></script>
<script>
  /*paypal.Buttons({
    createOrder: function (data, actions) {
        return actions.order.create({
            purchase_units : [{
                amount: {
                    currency_code: 'EUR',
                    value: '26'
                }
            }]
        });
    },
    onApprove: function (data, actions) {
        return actions.order.capture().then(function (details) {
            console.log(details)
            window.location.replace("payment_success.php?details="+encodeURIComponent(JSON.stringify(details)),true)
        })
    },
    onCancel: function (data) {
        window.location.replace("Oncancel.php")
    }
  }).render('#paypal-button-container');*/
</script>
 

</body>

<?php include "../Commons/footer.php";?>

</html>