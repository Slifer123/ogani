<?php

require_once "../../../bootstrap.php";
\Stripe\Stripe::setApiKey(STRIPE_API_KEY);

header('Content-type: text/html; charset=utf-8');
session_start();
if (isset($_POST['panier'])) {
    $data = $_POST['panier'];
}

$test =$_SESSION['total_panier'];

$YOUR_DOMAIN = HOST_NAME_PORT;
$YOUR_DOMAIN_PROD = HOST_NAME_PROD_URL;

$t =$_SESSION['total_panier']*100;

$checkout_session = \Stripe\Checkout\Session::create([
    'billing_address_collection' => 'required',
  'payment_method_types' => ['card'],
  'line_items' => [[
    'price_data' => [
      'currency' => 'eur',
      'unit_amount_decimal' =>$t,
      'product_data' => [
        'name' => 'Montant à payer',
        //'images' => ["http://localhost:8888/ogani/src/img/cart.png"],
      ],
    ],
    'quantity' => 1,
  ]],
  'mode' => 'payment',
  'success_url' => $YOUR_DOMAIN . 'ogani/src/traitements/traitement_commande.php?isCommandValidated=true',
  //'success_url' => $YOUR_DOMAIN_PROD . 'src/traitements/traitement_commande.php?isCommandValidated=true',
  'cancel_url' => $YOUR_DOMAIN . '/cancel.html',
]);

echo json_encode(['id' => $checkout_session->id]);

