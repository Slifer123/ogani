<?php include "../Commons/header.php";?>

    <?php echo formatTitrePageH3("Nom animal",COLOR_ASSO); ?>

   

    <div class="row mt-5 no-gutters perso_bgColorOrange border rounded border-dark align-items-center">

        <div class="col-12 col-md-3  col-lg-3 text-center">

            <img src="../../sources/images/Animaux/Chien/chien1.jpeg" class="img-thumbnail img-fluid" style="height:140px;"/>

        </div>

        <div class="col-12 col-md-1 col-lg-1 border-left border-right text-center border-dark">

            <img src="../../sources/images/Autres/icones/chienOk.png" class="img-fluid m-1" style="width:50px;"/>
            <img src="../../sources/images/Autres/icones/chatOk.png" class="img-fluid m-1" style="width:50px;"/>
            <img src="../../sources/images/Autres/icones/babyOk.png" class="img-fluid m-1" style="width:50px;"/>

        </div>

        <div class="col-12 col-md-3 col-lg-3 text-center border-dark">

            
                <div>Puce XXXXXXXXX</div>
                <div>Né XX XX XXXX</div>
                <div>
                    <span class="badge badge-warning">Douce</span>
                    <span class="badge badge-warning">Calme</span>
                    <span class="badge badge-warning">Joueuse</span>
                </div>

            
        </div>

        <div class="col-12 col-md-4 col-lg-4">

            Frais d'adoption: 60€.<br/>
            Vaccin: 35€ (à la demande de l'adoptant)<br/>
            Stérilisation : caution de 200€ vous sera demandée<br/>

            Bonne fin de soirée à tous et à bientôt pr la suite des aventures de mamie Chipie ❣️

        </div>
        
    </div>


    <div class="row mt-5  align-items-center">

        <div class="col-12 col-lg-4 text-center">


            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="../../sources/images/Animaux/Chien/chien1.jpeg" class="img-fluid" style="height:200px;"/>
                    </div>
                    <div class="carousel-item">
                        <img src="../../sources/images/Animaux/Chien/chien2.jpeg" class="img-fluid" style="height:200px;"/>
                    </div>
                    
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

        </div>

        <div class="col-12 col-lg-8">

            <?php echo formatTitrePageH3("Qui suis-je ?",COLOR_ASSO); ?>

            <div class="mt-5 perso_policeTitre">
                Un petit coucou de notre doyenne CHIPIE (20ans) en famille d accueil longue durée chez notre trésorière.<br/>
                La miss a un programme journalier surchargé :<br/>
                Manger, dormir, petit tour à la litière, retour dans le dodo juste devant le chauffage, câlins à ma Tatynou pour qu elle me donne des friandises et re dodo.
                Merci à tous pour les petits cadeaux, elle apprécie la gourmande 😉<br/><br/>

                Bonne fin de soirée à tous et à bientôt pr la suite des aventures de mamie Chipie ❣️
            </div>

            <div class="border-bottom border-dark mt-3 mb-3 mx-80%"></div>

            <div class="text-center">
                <img src="../../sources/images/Autres/icones/IconeAdopt.png" class="img-fluid  align-items-center" style="height:40px;"/>
            </div>

            <div class="perso_policeTitre">
                Un petit coucou de notre doyenne CHIPIE (20ans) en famille d accueil longue durée chez notre trésorière.<br/>
                La miss a un programme journalier surchargé :<br/>
                Manger, dormir, petit tour à la litière, retour dans le dodo juste devant le chauffage, câlins à ma Tatynou pour qu elle me donne des friandises et re dodo.
                Merci à tous pour les petits cadeaux, elle apprécie la gourmande 😉<br/><br/>

                Bonne fin de soirée à tous et à bientôt pr la suite des aventures de mamie Chipie ❣️
            </div>

            <div class="border-bottom border-dark mt-3 mb-3"></div>

            <div class="text-center">
                <img src="../../sources/images/Autres/icones/oeil.jpg" class="img-fluid  align-items-center" style="height:40px;"/>
            </div>

            <div class="perso_policeTitre">
                Un petit coucou de notre doyenne CHIPIE (20ans) en famille d accueil longue durée chez notre trésorière.<br/>
                La miss a un programme journalier surchargé :<br/>
                Manger, dormir, petit tour à la litière, retour dans le dodo juste devant le chauffage, câlins à ma Tatynou pour qu elle me donne des friandises et re dodo.
                Merci à tous pour les petits cadeaux, elle apprécie la gourmande 😉<br/><br/>

                Bonne fin de soirée à tous et à bientôt pr la suite des aventures de mamie Chipie ❣️
            </div>

            <div class="border-bottom border-dark mt-3 mb-3"></div>

            <div class="text-center">
                <img src="../../sources/images/Autres/icones/iconeContrat.png" class="img-fluid  align-items-center" style="height:40px;"/>
            </div>

            <div class="perso_policeTitre">
                Un petit coucou de notre doyenne CHIPIE (20ans) en famille d accueil longue durée chez notre trésorière.<br/>
                La miss a un programme journalier surchargé :<br/>
                Manger, dormir, petit tour à la litière, retour dans le dodo juste devant le chauffage, câlins à ma Tatynou pour qu elle me donne des friandises et re dodo.
                Merci à tous pour les petits cadeaux, elle apprécie la gourmande 😉<br/><br/>

                Bonne fin de soirée à tous et à bientôt pr la suite des aventures de mamie Chipie ❣️
            </div>

        </div>


    </div>


    







<?php include "../Commons/footer.php";?>