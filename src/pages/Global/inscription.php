<?php include "../Commons/header.php";?>

    <?php 
    if(isset($_POST['nom']) && !empty($_POST['nom']) && 
    isset($_POST['mail']) && !empty($_POST['mail']) &&
    isset($_POST['objet']) && !empty($_POST['objet']) &&
    isset($_POST['message']) && !empty($_POST['message']) &&
    isset($_POST['captcha']) && !empty($_POST['captcha'])
    ){
        $captcha = (int) $_POST['captcha'];
        if($captcha === 8){
            $nom = htmlentities($_POST['nom']);
            $mail = htmlentities($_POST['mail']);
            $objet = htmlentities($_POST['objet']);
            $message = htmlentities($_POST['message']);
            $destinataire = "kings@gmail.com";
            mail($destinataire, $objet. " - " . $nom, "Mail : ". $mail. " Message : " . $message);
            echo '<div class="alert alert-success" role="alert">';
                echo 'Message envoyé';
            echo '</div>';
        } else {
            echo '<div class="alert alert-danger" role="alert">';
            echo 'Erreur de Captcha, recommencer';
            echo '</div>';
        }
    }
    ?>

    <div class=" align-items-center center">
        <?= formatTitrePageH3("Créer un compte", "") ?>
        <div class="col-md-4 offset-md-4">
            <form method="POST" action="../../traitements/traitement_inscription.php" class="mt-3 pt-3 pl-3 pr-3 rounded perso_shadow">
                
                <div class="form-group row no-gutters align-items-center">
                    
                    <input type="text" name="nom" id="nom" class="form-control " Placeholder = "Nom" required/>
                </div>

                <div class="form-group row no-gutters align-items-center">
                    
                    <input type="email" name="email" id="mail" class="form-control " Placeholder = "Email" required/>
                </div>

                <div class="form-group row no-gutters align-items-center">
                    
                    <input type="password" name="password" id="password" class="form-control " Placeholder = "Mot de passe" required/>
                </div>

                
                <input type="submit" class="btn btn-primary mx-auto d-block" value="Valider" />

                
                    <p class="text-center mt-4">
                        Vous avez déjà un compte ? <a href="connexion.php" class="seconnecter">Se connecter</a>
                    </p>
                

            </form>
        </div>
    </div>




<?php include "../Commons/footer.php";?>