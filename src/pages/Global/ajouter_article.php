<?php include "../Commons/header.php";?>

    <div class="alert alert-success" id="articleAddedSuccess-alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <strong>Votre produit a été bien ajouté </strong>
    </div>

    <div class="alert alert-danger" id="logonRequired-alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <strong>Vous devez vous connecter avant de pouvoir ajouter un article </strong>
    </div>
    <!-- Hero Section Begin -->
    <section class="hero hero-normal">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="hero__categories">
                        <div class="hero__categories__all">
                            <i class="fa fa-bars"></i>
                            <span>Catégories</span>
                        </div>
                        <ul>
                            <li><a href="shop-grid.php?categorie=informatique">Informatique</a></li>
                            <li><a href="shop-grid.php?categorie=console_jeu">Console & Jeux vidéo</a></li>
                            <li><a href="shop-grid.php?categorie=image_son">Image & Son</a></li>
                            <li><a href="shop-grid.php?categorie=telephonie">Téléphonie</a></li>
                            <li><a href="shop-grid.php?categorie=vetement">Vêtements</a></li>
                            <li><a href="shop-grid.php?categorie=chaussure">Chaussures</a></li>
                            <li><a href="shop-grid.php?categorie=accessoire_bagagerie">Accessoires et Bagagerie</a></li>
                            <li><a href="shop-grid.php?categorie=montre_bijou">Montres & Bijoux</a></li>
                            <li><a href="shop-grid.php?categorie=equipement_bebe">Equipements bébé</a></li>
                            <li><a href="shop-grid.php?categorie=vetement_bebe">Vêtements bébé</a></li>
                            <li><a href="shop-grid.php?categorie=luxe_tendance">Luxe & Tendances</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-9">
                <div class="hero__search">
                        <div class="hero__search__form">
                            <form method="post" action="shop-grid.php">
                                <input type="text" name="recherche" placeholder="Recherche">
                                <button type="submit" class="site-btn"><i class="fas fa-search"></i></button>
                            </form>
                        </div>
                       
                    </div>
    
                </div>
            </div>
        </div>
    </section>
    <!-- Hero Section End -->

    <div class=" align-items-center center">
        <?= formatTitrePageH3("Ajouter article", "") ?>

        

        <div class="col-lg-10 offset-lg-1 pt-2">
            <form action="../../traitements/traitement_ajouter_article.php" enctype="multipart/form-data" class="dropzone rounded" id="mDropzone">
                <div class="dz-message" data-dz-message><span>Cliquer ici pour ajouter des photos</span></div>
            </form> 

            <!-- Custom icons for docx, xlsx, pptx, jpg, pdf, zip 
            <div class="file-loading">
                <input id="input-ficons-1" name="input-ficons-1[]" multiple type="file">
            </div>-->
            


            <form method="POST" action="../../traitements/traitement_ajouter_article.php" enctype="multipart/form-data" class="mt-3 pt-3 pl-3 pr-3 pb-3 rounded perso_shadow" id="form">
                
                <!--<div class="form-group row no-gutters align-items-center">
                    
                    <input type="file" class="form-control-file" id="file-input" multiple> id="form"
                    <div class="pt-2" id="preview"></div>
                </div>-->

                

                <!--<div class="dropzone" id="myDropzone"></div>-->

                <div class="form-group ">
                    
                    <input type="text" name="article_titre" id="titre" class="form-control" Placeholder = "Titre" required/>
                    
                </div>

                <div class="form-group">
                    
                    <textarea class="form-control" name="article_description" Placeholder = "Description" id="description" rows="3" required></textarea>
                </div>

                
                
    
                <div class="form-group">
                    
                    <select class="form-control selectpicker border " title="Catégorie" style="" data-size="10" name="article_categorie" id="mySelect">
                        
                
                        <optgroup data-icon="fas fa-mobile-alt" label="MULTIMEDIA">
                            <option value="informatique">Informatique</option>
                            <option value="console_jeu">Console & Jeux vidéo</option>
                            <option value="image_son">Image & Son</option>
                            <option value="telephonie">Téléphonie</option>
                        </optgroup>

                        <optgroup data-icon="fas fa-tshirt" label="CAMPING">
                            <option value="vetement">Vêtements</option>
                            <option value="chaussure">Chaussures</option>
                            <option value="accessoire_bagagerie">Accessoires et Bagagerie</option>
                            <option value="montre_bijou">Montres & Bijoux</option>
                            <option value="equipement_bebe">Equipements bébé</option>
                            <option value="vetement_bebe">Vêtements bébé</option>
                            <option value="luxe_tendance">Luxe & Tendances</option>
                        </optgroup>

                    </select>
                </div>

                <div class="form-group">
                    
                    <select class="form-control border selectpicker" title="Etat" style="" name="article_etat" id="exampleFormControlSelect1">
                    
                        <option>Neuf</option>
                        <option>D'occasion</option>
                    
                    </select>
                </div>
                
                

                

                <div class="form-group row no-gutters align-items-center">
                    
                    <input type="text" onfocus="(this.type='date')" onblur="(this.type='text')" name="article_date_achat" id="date" class="form-control" style="color:red;" Placeholder = "Date d'achat"/>
                    <div id="jsValue" style="color:red;font-size:12px;"> 
                    
                    </div>
                </div>

                <div class="form-group row no-gutters align-items-center">
                    
                    <input type="number" name="article_prix_achat" id="prix_achat" class="form-control " Placeholder = "Prix d'achat" required/>
                </div>

                <div class="form-group row no-gutters align-items-center">
                    
                    <input type="number" name="article_prix_vente" id="prix_vente" class="form-control " Placeholder = "Prix de vente" required/>
                </div>
                
                <div class="form-group row no-gutters align-items-center">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" name="preuve_achat" id="selectedFile">
                        <label class="custom-file-label" for="selectedFile">Preuve d'achat</label>
                    </div>
                </div>

                    
                
                <input type="submit" id="submit-all" class="btn btn-primary mx-auto d-block" value="Ajouter" />
                

            </form>

            

    
                    

           
        </div>
    </div>



<script>


    const _MS_PER_DAY = 1000 * 60 * 60 * 24;

    // a and b are javascript Date objects
    function dateDiffInDays(a, b) {
    // Discard the time and time-zone information.
    const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
    const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

    return Math.floor((utc2 - utc1) / _MS_PER_DAY);
    }


    Dropzone.options.myDropzone= {
    url: "../../traitements/traitement_ajouter_article.php",
    autoProcessQueue: true,
    uploadMultiple: true,
    parallelUploads: 5,
    maxFiles: 5,
    maxFilesize: 10,
    acceptedFiles: 'image/*',
    dictDefaultMessage : 'Change the text here!',
    addRemoveLinks: true,
    init: function() {
        dzClosure = this; // Makes sure that 'this' is understood inside the functions below.

        // for Dropzone to process the queue (instead of default form behavior):
        document.getElementById("submit-all").addEventListener("click", function(e) {
            // Make sure that the form isn't actually being sent.
            e.preventDefault();
            e.stopPropagation();
            dzClosure.processQueue();
        });

        //send all the form data along with the files:
        /*this.on("sendingmultiple", function(data, xhr, formData) {
            formData.append("email", jQuery("#email").val());
            formData.append("password", jQuery("#password").val());
        });*/
    }
}

document.getElementById('date').addEventListener("change", e => {
  e.preventDefault();

  var date = document.getElementById('date');
  var message = document.getElementById('jsValue');
  
  
  console.log("Date : " + date.value);
  
  var today = new Date();
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = today.getFullYear();

  today = yyyy + '-' + mm + '/' + dd;

  a = new Date(date.value);
  b = new Date(today);
  
  difference = dateDiffInDays(a, b);
  console.log("difference : " + dateDiffInDays(a, b));

  if (difference > 30) {
    console.log("produit dépasse 90 j");
    document.getElementById("submit-all").disabled = true;
    message.innerHTML = "La date d'achat doit être inférieure ou égale à 90 jours";

    //e.preventDefault();
  } else {
    console.log("Dates filled.");
    document.getElementById("submit-all").disabled = false;
    message.innerHTML = "";
  }
})

document.getElementById('selectedFile').addEventListener('change',function(){
    //get the file name
    var fileName = $(this).val().replace('C:\\fakepath\\', " ");
    //replace the "Choose a file" label
    $(this).next('.custom-file-label').html(fileName);
})



</script>
<?php include "../Commons/footer.php";?>