<?php include "../Commons/header.php";?>

<?php

use Site\Entity\Personne;
use Site\Entity\Article;
use Site\Entity\Image;
use Doctrine\Common\Collections\ArrayCollection; 

//$entityManager = require_once "../../../bootstrap.php";
//echo "toto";
     $nbArticles =0;
    // On détermine sur quelle page on se trouve
    if(isset($_GET['page']) && !empty($_GET['page'])){
        $currentPage = (int) strip_tags($_GET['page']);
    }else{
        $currentPage = 1;
    }

    if(isset($_GET["categorie"]) && !empty($_GET["categorie"]))
    {
        $nbArticles = getTotalNumberOfAllArticlesByCategorie($entityManager,$_GET["categorie"])->getSingleScalarResult();
    }
    else{
        $nbArticles = getTotalNumberOfAllArticles($entityManager)->getSingleScalarResult();
    }

    

    //echo "count ".$nbArticles;

    // On détermine le nombre d'articles par page
    $parPage = 9;

    // On calcule le nombre de pages total
    $pages = ceil($nbArticles / $parPage);

    // Calcul du 1er article de la page
    $premier = ($currentPage * $parPage) - $parPage;
    //echo $pages;

    $queryBuilder = $entityManager->createQueryBuilder();

    //Lorsqu'on recherche un article
    if(isset($_POST["recherche"]) && !empty($_POST["recherche"]))
    {
        $query = getAllArticlesThatContainsASpecificValue($entityManager,$_POST["recherche"],$premier,$parPage);
    } 
    //Lorsqu'on recherche une categorie
    elseif (isset($_GET["categorie"]) && !empty($_GET["categorie"]))
    {
        //echo "on entre".$_GET["categorie"];
        $query = getAllArticlesWithASpecificCategorie($entityManager,$_GET["categorie"],$premier,$parPage);
    }
    //Quand on clique sur le lien boutique
    else {

        $queryBuilder->select('a')
        ->from(Article::class, 'a')
        ->addOrderBy('a.id_article', 'DESC')
        ->setFirstResult($premier)
        ->setMaxResults($parPage);
        
        
        $query = $queryBuilder->getQuery();
    }
    
    

?>

<!-- Hero Section Begin -->
    <section class="hero hero-normal">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="hero__categories">
                        <div class="hero__categories__all">
                            <i class="fa fa-bars"></i>
                            <span>Catégories</span>
                        </div>
                        <ul>
                            <li><a href="shop-grid.php?categorie=informatique">Informatique</a></li>
                            <li><a href="shop-grid.php?categorie=console_jeu">Console & Jeux vidéo</a></li>
                            <li><a href="shop-grid.php?categorie=image_son">Image & Son</a></li>
                            <li><a href="shop-grid.php?categorie=telephonie">Téléphonie</a></li>
                            <li><a href="shop-grid.php?categorie=vetement">Vêtements</a></li>
                            <li><a href="shop-grid.php?categorie=chaussure">Chaussures</a></li>
                            <li><a href="shop-grid.php?categorie=accessoire_bagagerie">Accessoires et Bagagerie</a></li>
                            <li><a href="shop-grid.php?categorie=montre_bijou">Montres & Bijoux</a></li>
                            <li><a href="shop-grid.php?categorie=equipement_bebe">Equipements bébé</a></li>
                            <li><a href="shop-grid.php?categorie=vetement_bebe">Vêtements bébé</a></li>
                            <li><a href="shop-grid.php?categorie=luxe_tendance">Luxe & Tendances</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="hero__search">
                        <div class="hero__search__form">
                            <form method="post" action="">
                                <input type="text" name="recherche" placeholder="Recherche">
                                <button type="submit" class="site-btn"><i class="fas fa-search"></i></button>
                            </form>
                        </div>
                       
                    </div>
    
                </div>
            </div>
        </div>
    </section>
    <!-- Hero Section End -->


    <!-- Product Section Begin -->
    <section class="product spad">
        <div class="container">
            <div class="row">
                
                <div class="col-lg-12 col-md-12">
                    
                    
                    <div class="row">

                        <?php 
                        if (count($query->getResult())!=0) {
                            count($query->getResult());
                            foreach ($query->getResult() as $articles) {
                                //foreach ($articles->getImages() as $images) {
                                    //echo "../".$images->getUrl();
                                    //$url = explode( '../upload/', $images->getUrl() );
                                    //echo $url[1];
                                    //var_dump( explode( '../upload/', $images->getUrl() ) );
                                    //echo "ici ".$articles->getImages()->first()->getUrl();
                                    //var_dump($articles->getImages()->first());
                                    $url = explode( '../upload/', $articles->getImages()->first()->getUrl() );
                                    
                                //foreach ($articles->getImages() as $image) {
                                // $url = explode( '../upload/', $image->getUrl() );
                                

                                

                        ?>
                            <div class="col-lg-4 col-md-6 col-sm-6">
                                <div class="product__item">
                                    <a href=<?php echo "shop-details.php?idArticle=".$articles->getId();?>>
                                        <div class="product__item__pic set-bg" data-setbg=<?php echo "../../upload/image/".$url[1];?>>
                                            <ul class="product__item__pic__hover">
                                                <!--<li><a href="#"><i class="fa fa-heart"></i></a></li>
                                                <li><a href="#"><i class="fa fa-retweet"></i></a></li>
                                                <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>-->
                                            </ul>
                                        </div>
                                    </a>
                                    <div class="product__item__text">
                                        <h6><a href=<?php echo "shop-details.php?idArticle=".$articles->getId();?>><?php echo $articles->getTitre();?></a></h6>
                                        <h5><?php echo $articles->getPrixVente()." €";?></h5>
                                    </div>
                                </div>
                            </div>

                        <?php
                            //}
                            //}
                               }
                            }
                            //S'il n'y a pas d'article pour la catégorie selectionnée
                            else {
                                //echo "aucun artilce";
                           
                        ?>

                            <div class="col-lg-12 col-md-12 col-sm-12">
                                
                                <h3>Aucun produit de la catégorie selectionnée n'est disponible</h3>

                                
                            </div>


                        <?php



                            }
                        ?>

                    
                        

                    </div>
                    <?php

                        //On affiche la pagination que lorsqu'on a des articles
                        if (count($query->getResult())!=0) {
                            
                    ?>
                    <ul class="pagination ">
                        <li class="page-item <?= ($currentPage == 1) ? "disabled" : "" ?>">
                            <a <?php if (isset($_GET['categorie']))
                                    {
                                        echo "href='?page=".($currentPage - 1 )."&categorie=".$_GET['categorie']."' class='page-link'";
                                    }
                                    else{
                                        echo "href='?page=".($currentPage - 1 )."' class='page-link'";
                                    }
                                        
                                ?>>
                                <span aria-hidden="true">&laquo;</span>
                                <span class="sr-only">Previous</span>
                            </a>
                        </li>
                        <?php for($page = 1; $page <= $pages; $page++): ?>
                          <!-- Lien vers chacune des pages (activé si on se trouve sur la page correspondante) -->
                          <li class="page-item <?= ($currentPage == $page) ? "active" : "" ?>">
                                <a <?php if (isset($_GET['categorie']))
                                    {
                                        echo "href='?page=".$page."&categorie=".$_GET['categorie']."' class='page-link'";
                                    }
                                    else{
                                        echo "href='?page=".$page."' class='page-link'";
                                    }
                                        
                                ?>
                                >
                                    <?= $page ?>
                                </a>
                            </li>
                        <?php endfor ?>
                          <!-- Lien vers la page suivante (désactivé si on se trouve sur la dernière page) -->
                          <li class="page-item <?= ($currentPage == $pages) ? "disabled" : "" ?>">
                            <a <?php if (isset($_GET['categorie']))
                                    {
                                        echo "href='?page=".($currentPage + 1 )."&categorie=".$_GET['categorie']."' class='page-link'";
                                    }
                                    else{
                                        echo "href='?page=".($currentPage + 1 )."' class='page-link'";
                                    }
                                        
                                ?>>
                            
                                <span aria-hidden="true">&raquo;</span>
                                <span class="sr-only">Next</span>
                            </a>
                            
                        </li>
                            
                    </ul>
                    <?php


                        }
                            
                    ?>
                </div>
            </div>
        </div>
    </section>
    <!-- Product Section End -->

    



</body>

<?php include "../Commons/footer.php";?>

</html>