<?php 
require  "../../utile/config.php";
require "../../utile/requetage.php";

$entityManager = require "../../../bootstrap.php";

use Twilio\Rest\Client;
use Site\Entity\Personne;

/*$verifyToken = '360992';
$email = 'stevezanou@gmail.com';*/
session_start();
$verifyToken = $_GET['token'] ?? false;
$email = $_GET['mail'] ?? false;
// add validation of code and email for production systems!
//die("on entre".$email);
if (!$email || !$verifyToken) 
{
   throw new \Exception('Email or code not set');
}

//die($verifyToken);
$sid = TWILIO_ACCOUNT_SID;
$token = TWILIO_AUTH_TOKEN;
$client = new Client($sid, $token);

// send the verification check request to the Twilio API
try {
   $verification = $client->verify
       ->v2
       // service id of the verification service we created - we'll probably want this
       // stored in a config file somewhere
       ->services(TWILIO_VERIFY_SERVICE_SID)
       ->verificationChecks
       ->create($verifyToken, ["to" => $email]);
   // update your user in the database to set the verified flag

   if ($verification->status === 'approved') {
       $message = 'Vous pouvez maintenant vous connecter';

       $personneRepo = $entityManager->getRepository(Personne::class);

       $personneByEmail = getPersonneByEmail($personneRepo,$email);

        if($personneByEmail != null)
        {
            //echo "la date".$personneByEmail[0]->getIsMailValidated();
            $personneByEmail[0]->setIsMailValidated(true);
            //echo "l'email".$personneByEmail[0]->getIsMailValidated();
            $entityManager->persist($personneByEmail[0]);
            $entityManager->flush();
            header('Location: connexion.php?isMailVerified=true');
            //unset($_SESSION['email_address_verify']);
        }                                                  
       
   } else {
       $message = 'Erreur code invalide';
       header('Location: connexion.php?isMailCodeNotValid=true');
   }
} catch (\Twilio\Exceptions\RestException $e) {
   $message = 'Erreur votre code a expiré';
   header('Location: connexion.php?isMailCodeNotValid=true');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <title></title>
   <style>
       html, body {
           height: 100%;
           width: 100%;
       }

       body, body {
           display: flex;
       }

       h1 {
           margin: auto;
       }
   </style>
</head>
<body>
<h1><?= $message; ?></h1>
</body>
</html>