<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>Button Example Code</title>

<!-- START META TAG SECTION -->
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="Content-Language" content="en">
<!-- END META TAG SECTION -->
   
</head>

<body>

<!-- START SAMPLE CODE SECTION -->


<!-- PARAGRAPH 1 -->
Basic Examples

<br><br>

Add to Cart and Buy Now Example, Multiple Text Input.

<br><br>

Notes:
<br>
* This example demonstrates how to do the same thing using either a Buy Now Item Button or Add to Cart Item Button.


<br><br>
<hr align="left" width="50%" noshade>
<br><br>

<!-- START CONTENTS -->

<!-- START BUTTON EXAMPLES -->

<!-- START BUTTON EXAMPLES -->

<!-- Start of Buy Now Form -->
Widget Statue Trophy - $100.00 plus $20.00 shipping

<br><br>

<!-- Start of Buy Now Form -->
<form target="_self" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
<!-- If using a Business or Company Logo Graphic, include the "cpp_header_image" variable in your View Cart code. -->
<input type="hidden" name="cpp_header_image" value="https://yourwebsite.com/logo.jpg">

<input type="hidden" name="cmd" value="_cart">
<input type="hidden" name="upload" value="1">
<!-- Replace "business" value with your PayPal Email Address or Account ID -->
<input type="hidden" name="business" value="sb-rfcbs5214405@business.example.com">
<!-- Specify details about the item that buyers will purchase. -->
  <input type="hidden" name="item_name_1" value="Premium Umbrella">
  <input type="hidden" name="amount_1" value="50.00">
  <input type="hidden" name="quantity_1" value="1">

  <input type="hidden" name="item_name_2" value="Premium Umbrella">
  <input type="hidden" name="amount_2" value="50.00">
  <input type="hidden" name="quantity_2" value="1">

  <input type="hidden" name="currency_code" value="EUR">
  <INPUT TYPE="hidden" name="address_override" value="1">
<!-- Replace value with the web page you want the customer to return to after a successful transaction -->
<input type="hidden" name="return" value="association.php">
<!-- Replace value with the web page you want the customer to return to after item cancellation -->
<input type="hidden" name="cancel_return" value="http://www.yourwebsite.com/Cancel.html">
<!-- Note: shipping override variable is used with this example -->
<input type="hidden" name="shipping" value="20.00">
<input type="hidden" name="button_subtype" value="products">
<input type="hidden" name="no_note" value="0">
<input type="hidden" name="cn" value="Add special instructions to the seller:">
<input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynow_LG.gif:NonHosted">

<input type="text" name="first_name" >
  <input type="text" name="last_name" >
  <input type="text" name="address1" >
  <input type="text" name="city" >
  
  <input type="text" name="zip" >
  <input type="text" name="country">

<!-- -->
<input type="image" src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/PP_logo_h_100x26.png" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
</form>
<!-- End of Buy Now Form -->


<br><br>
<hr align="left" width="50%" noshade>
<br><br>



<!-- Start of Add to Cart Form -->
Widget Statue Trophy - $100.00 plus $20.00 shipping

<br><br>

<!-- Note: target="_self" was replaced with the variable target="_self" -->
<!-- Note: shopping_url also added to code -->
<!-- These two changes allow better functionality with IE and Firefox --> 
<form target="_self" action="https://www.paypal.com/cgi-bin/webscr" method="post">
<!-- If using a Business or Company Logo Graphic, include the "cpp_header_image" variable in your View Cart code. -->
<input type="hidden" name="cpp_header_image" value="https://yourwebsite.com/logo.jpg"> 
<input type="hidden" name="cmd" value="_cart">
<input type="hidden" name="add" value="1">
<!-- Replace "business" value with your PayPal Email Address or Account ID -->
<input type="hidden" name="business" value="your email address">
<input type="hidden" name="item_name" value="Widget Statue">
<input type="hidden" name="item_number" value="WS-1001">
<input type="hidden" name="amount" value="100.00">
<input type="hidden" name="lc" value="US">
<input type="hidden" name="currency_code" value="USD">
<input type="hidden" name="no_shipping" value="2">
<input type="hidden" name="button_subtype" value="products">
<input type="hidden" name="no_note" value="0">
<input type="hidden" name="cn" value="Add special instructions to the seller:">
<input type="hidden" name="bn" value="PP-ShopCartBF:btn_cart_LG.gif:NonHosted">
<!-- Replace value with the web page you want the customer to return to -->
<input type="hidden" name="shopping_url" value="http://www.yourwebsite.com/Shop.html">
<!-- Replace value with the web page you want the customer to return to after a successful transaction -->
<input type="hidden" name="return" value="http://www.yourwebsite.com/ThankYou.html">
<!-- Replace value with the web page you want the customer to return to after item cancellation -->
<input type="hidden" name="cancel_return" value="http://www.yourwebsite.com/Cancel.html">
<!-- Note: shipping override variable is used with this example -->
<input type="hidden" name="shipping" value="20.00">
<!-- -->
Name Plate Engraving Information:
<br><br>

First Name:&nbsp;&nbsp;
<input type="hidden" name="on0" value="First Name">
<input type="text" name="os0" size="20">
<br><br>
Last Name:&nbsp;&nbsp;
<input type="hidden" name="on1" value="Last Name">
<input type="text" name="os1" size="20">
<br><br>
School:&nbsp;&nbsp;
<input type="hidden" name="on2" value="School">
<input type="text" name="os2" size="20">
<br><br>
Event:&nbsp;&nbsp;
<input type="hidden" name="on3" value="Event">
<input type="text" name="os3" size="20">
<br><br>
Date:&nbsp;&nbsp;
<input type="hidden" name="on4" value="Date">
<input type="text" name="os4" size="8">
<br><br>
Team Name:&nbsp;&nbsp;
<input type="hidden" name="on5" value="Team Name">
<input type="text" name="os5" size="20">
<br><br>
Place:&nbsp;&nbsp;
<input type="hidden" name="on5" value="Place">
<input type="text" name="os5" size="20">
<br><br>
<!-- -->
<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_cart_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
</form>
<!-- End of Add to Cart Form -->

<!--  Start of View Cart Button Code  -->
<form target="_self" action="https://www.paypal.com/cgi-bin/webscr" method="post">
<!-- If using a Business or Company Logo Graphic, include the "cpp_header_image" variable in your View Cart code. -->
<input type="hidden" name="cpp_header_image" value="https://yourwebsite.com/logo.jpg"> 
<input type="hidden" name="cmd" value="_cart">
<input type="hidden" name="display" value="1">
<!-- Replace "business" value with your PayPal Email Address or Account ID -->
<input type="hidden" name="business" value="your email address">
<!-- Replace value with the web page you want the customer to return to -->
<input type="hidden" name="shopping_url" value="http://www.yourwebsite.com/Shop.html">
<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_viewcart_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
</form> 
<!--  End of View Cart Button Code  -->



<!-- END BUTTON EXAMPLES -->



<!-- END SAMPLE CODE SECTION -->

<br><br>


<br><br><br><br>
<hr align="left" width="50%" noshade>
<br><br>
NOTES:
<br>
In order to test the code, you must replace the &quot;business&quot; value variable with your PayPal Email Address or Account ID.


</body>
</html>