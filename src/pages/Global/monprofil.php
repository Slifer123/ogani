<?php include "../Commons/header.php";
use Site\Entity\Personne;
use Site\Entity\Article;
use Site\Entity\Image;
session_start();
//echo "ici".$_SESSION['mail'];
$entityManager = require_once "../../../bootstrap.php";
$personneRepo = $entityManager->getRepository(Personne::class);

$personneByEmailAndPassword = $personneRepo->findBy(["email_personne" => $_SESSION['mail'], 
                                                     "mdp_personne" => $_SESSION['mdp']
                                                    ]
                                                    );

//var_dump($personneByEmailAndPassword[0]->getProfilePicture());
if(isset($_POST) && isset($_POST['save_profile']))
{
    
    
    
    //echo "on entre";
    //session_start();
   // echo $_SESSION['mail'];
    extract($_POST);
    //var_dump($_FILES);

    
    //var_dump($personneByEmailAndPassword[0]);

    $target_dir = "../../upload/profile/";

    $target_file = $target_dir . basename($_FILES["profile_photo"]["name"]);

    $personneByEmailAndPassword[0]->setProfilePicture($target_file);

    $entityManager->persist($personneByEmailAndPassword[0]);
        //$entityManager->persist($_SESSION['personne']);
        //echo "Identifiant5";
    $entityManager->flush();

    move_uploaded_file($_FILES["profile_photo"]["tmp_name"], $target_dir.$_FILES['profile_photo']['name']) ;

        
}

$queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('p')
        ->from(Personne::class, 'p')
        ->join(Article::class,'a')
        ->join(Image::class,'i')
        ->where('p.id_personne = :id_personne')
        ->setParameter('id_personne', $personneByEmailAndPassword[0]->getId());
        

        $query = $queryBuilder->getQuery();


?>

<div class="row align-items-center no-gutters">
    <div class="col-12 col-lg-3 p-2" >
        <div id="profile-container fixed-top">
            <form action="" method="post" enctype="multipart/form-data">
                <img id="profileImage" name = "profile_picture" class="img-fluid img-thumbnail imageArticle" src=<?php 
                                                                                                         if(empty($personneByEmailAndPassword[0]->getProfilePicture()))
                                                                                                         {
                                                                                                             echo "../../sources/images/Autres/profilnotfound.png";
                                                                                                         }  
                                                                                                         else
                                                                                                         {
                                                                                                             echo $personneByEmailAndPassword[0]->getProfilePicture(); 
                                                                                                         }
                                                                                                    ?> alt='Profile Picture'/>
                <input id="imageUpload" type="file" name="profile_photo" placeholder="Photo" required="" capture>
                <button id="uploadProfileImage" type="submit" name="save_profile" class="btn btn-primary btn-block">Save User</button>
            </form>
        </div>
       
    </div>
    <div class="col-12 col-lg-9 p-2" >
        
        <div class="mt-5">

            <div class="row mt-5 justify-content-center">
                <?php 
                    foreach ($query->getResult() as $user) {
                        foreach ($user->getArticles() as $articles) {
                            //foreach ($articles->getImages() as $images) {
                                //echo "../".$images->getUrl();
                                //$url = explode( '../upload/', $images->getUrl() );
                                //echo $url[1];
                                //var_dump( explode( '../upload/', $images->getUrl() ) );
                                //echo "ici ".$articles->getImages()->first()->getUrl();
                                $url = explode( '../upload/', $articles->getImages()->first()->getUrl() );
                ?>
                                <div class="col-auto pt-2">
                                    <!--<div class="card h-100  mx-auto" style="width: 18rem;">
                                        <div class="card-header text-center">
                                            Client
                                        </div>
                                        <img src=<?php //echo "../../upload/image/".$url[1];?>  class="card-img-top imageArticle" alt="...">
                                        <div class="card-footer text-center text-muted">
                                            
                                            <p class="card-text">12€</p>
                                            <p class="card-text">Bose</p>
                                            
                                        </div>
                                    </div>-->

                                    <div class="card-deck h-100 mx-auto">
                                        <div class="card">
                                            <a href=<?php echo "afficherArticle.php?idArticle=".$articles->getId();?>><img src=<?php echo "../../upload/image/".$url[1];?>  class="card-img-top imageArticle" alt="..."></a>
                                            <div class="card-body">
                                            
                                                <p class="card-text text-center"><?php echo $articles->getPrixVente()." €";?></p>
                                            
                                            </div>
                                        </div>
                                    
                                    </div>

                                </div>

                               
                <?php
                            //}
                        }
                    }
                ?>

                
            </div>
                

            


        </div>
    </div>
</div>

<?php 

include("../Commons/footer.php"); 
?>
            
      