<?php include "../Commons/header.php";

use Site\Entity\Personne;
use Site\Entity\Article;
use Site\Entity\Image;
session_start();
//echo "ici".$_SESSION['mail'];
//$entityManager = require_once "../../../bootstrap.php";
$personneRepo = $entityManager->getRepository(Personne::class);

$personneByEmailAndPassword = getPersonneByEmailAndPassword($personneRepo,$_SESSION['mail'],$_SESSION['mdp']);

\Stripe\Stripe::setApiKey('pk_test_51HLaEDC6tvxFIDbHeRKVQ9iYXhO5SgspSbHXw2BwZklunWY9qwtV18YBDye1MxmsocxarpqCgDdqTifyvJzeI1dC00iCqC1ndC');

$status="";
//var_dump($_SESSION["shopping_cart"]);

/*if (isset($_POST['action']) && $_POST['action']=="remove"){

    echo "on entre ici".$_POST["id"];
    var_dump($_POST);
    
   if(!empty($_SESSION["shopping_cart"])) {
     //var_dump($_SESSION["shopping_cart"] );
     //die();
       foreach($_SESSION["shopping_cart"] as $key => $value) {
       // var_dump($value);
        //die();
         if($_POST["id"] == $value['id']){
             print_r($_SESSION["shopping_cart"]);
         unset($_SESSION["shopping_cart"][$key]);
        
         //die();
         $status = "<div class='box' style='color:red;'>
             Ce produit a été retiré de votre panier</div>";
         }
         if(empty($_SESSION["shopping_cart"]))
         unset($_SESSION["shopping_cart"]);
     }		
   }
}*/



if (isset($_POST['action']) && $_POST['action']=="change"){
    //echo "on entre".$_POST["id"];
  foreach($_SESSION["shopping_cart"] as &$value){
      
    if($value['id'] == $_POST["id"]){
        //echo "id".$value['id'];
        $value['quantity'] = $_POST["quantity"];
        //echo "change".$value['quantity'];
        break; // Stop the loop after we've found the product
    }
  }
  	
}

if (isset($_POST['reduction']) && !empty($_POST['reduction'])){

    //$reduction = $_POST['reduction'];
    $reduction = 10;

}


?>

    <!-- Hero Section Begin -->
    <!-- Hero Section Begin -->
    <section class="hero hero-normal">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="hero__categories">
                        <div class="hero__categories__all">
                            <i class="fa fa-bars"></i>
                            <span>Catégories</span>
                        </div>
                        <ul>
                            <li><a href="shop-grid.php?categorie=informatique">Informatique</a></li>
                            <li><a href="shop-grid.php?categorie=console_jeu">Console & Jeux vidéo</a></li>
                            <li><a href="shop-grid.php?categorie=image_son">Image & Son</a></li>
                            <li><a href="shop-grid.php?categorie=telephonie">Téléphonie</a></li>
                            <li><a href="shop-grid.php?categorie=vetement">Vêtements</a></li>
                            <li><a href="shop-grid.php?categorie=chaussure">Chaussures</a></li>
                            <li><a href="shop-grid.php?categorie=accessoire_bagagerie">Accessoires et Bagagerie</a></li>
                            <li><a href="shop-grid.php?categorie=montre_bijou">Montres & Bijoux</a></li>
                            <li><a href="shop-grid.php?categorie=equipement_bebe">Equipements bébé</a></li>
                            <li><a href="shop-grid.php?categorie=vetement_bebe">Vêtements bébé</a></li>
                            <li><a href="shop-grid.php?categorie=luxe_tendance">Luxe & Tendances</a></li>
                        </ul>
                </div>
                </div>
                <div class="col-lg-9">
                    <div class="hero__search">
                        <div class="hero__search__form">
                            <form method="post" action="shop-grid.php">
                                <input type="text" name="recherche" placeholder="Recherche">
                                <button type="submit" class="site-btn"><i class="fas fa-search"></i></button>
                            </form>
                        </div>
                       
                    </div>
    
                </div>
            </div>
        </div>
    </section>
    <!-- Hero Section End -->
    <!-- Hero Section End -->

    <div class="alert alert-success" id="isCommandTerminatedSuccess-alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <strong>Votre commande a été bien prise en compte</strong>
    </div>
    <!-- Shoping Cart Section Begin -->
    <section class="shoping-cart spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="shoping__cart__table">
                    <?php
                        if(isset($_SESSION["shopping_cart"])){
                            $total_price = 0;
                            $tva=0;
                            $basket_price=0;
                    ?> 
                        <table>
                            <thead>
                                <tr>
                                    <th class="shoping__product">Produit</th>
                                    <th>Prix</th>
                                    <th>Quantité</th>
                                    <th>Total</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                                foreach ($_SESSION["shopping_cart"] as $product){

                                    $url = explode( '../upload/', $product["image"]);
                            ?>
                                <tr>
                                    <td class="shoping__cart__item">
                                        <img src=<?php echo "../../upload/image/".$url[1]; ?> alt="" style=" height: 50px;width: 50px;">
                                        <h5><?php echo $product["titre"]; ?></h5>
                                    </td>
                                    <td class="shoping__cart__price">
                                        
                                        <?php echo $product["prixVente"]; ?>
                                    </td>
                                    <td class="shoping__cart__quantity">
                                        <div class="quantity d-flex justify-content-center ">
                                            <form method='post' action=''>
                                                <input type='hidden' name='id' value="<?php echo $product["id"]; ?>" />
                                                <input type='hidden' name='action' value="change" />
                                                <select name='quantity' class='quantity' onChange="this.form.submit()">
                                                    <option <?php if($product["quantity"]==1) echo "selected";?>
                                                    value="1">1</option>
                                                    <option <?php if($product["quantity"]==2) echo "selected";?>
                                                    value="2">2</option>
                                                    <option <?php if($product["quantity"]==3) echo "selected";?>
                                                    value="3">3</option>
                                                    <option <?php if($product["quantity"]==4) echo "selected";?>
                                                    value="4">4</option>
                                                    <option <?php if($product["quantity"]==5) echo "selected";?>
                                                    value="5">5</option>
                                                </select>
                                            </form>
                                        </div>
                                    </td>
                                    <td class="shoping__cart__total">
                                        <?php echo $product["prixVente"]*$product["quantity"]." €"; ?>
                                    </td>
                                    <td class="shoping__cart__item__close ">
                                        <form method='post' action='' id="removeForm">
                                            <input type='hidden' name='id' value="<?php echo $product['id']; ?>" />
                                            <input type='hidden' name='action' value="remove" />
                                            <button type='submit' class='remove'><span class="icon_close"></span></button>
                                        </form>
                                        
                                    </td>
                                </tr>
                            <?php 

                                    $total_price += ($product["prixVente"]*$product["quantity"]);
                                }
                            ?>
                            </tbody>
                        </table>
                        <?php
                            }
                            else
                            {
                            
                                echo "<h3>Votre panier est vide</h3>";
                            }
                        ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="shoping__cart__btns">
                        <a href="shop-grid.php" class="primary-btn cart-btn">CONTINUER MES ACHATS</a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="shoping__continue">
                        <div class="shoping__discount">
                            <h5>Coupon de réduction</h5>
                            <form method="post" action="">
                                <input type="text" name="reduction" placeholder="Entrez votre coupon">
                                <button type="submit" class="site-btn">APPLIQUER</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="shoping__checkout">
                        <h5>Total du panier</h5>
                        <ul>
                            <li>Sous Total <span><?php
            
                                                    if(isset($total_price)){
                                                        echo $total_price." €";
                                                    }
                                                    else
                                                    {
                                                        echo "0";
                                                    }
                                                ?>
                                            </span>
                            </li>
                            <li>Total TTC<span>
                                            <?php
                                                $tva;
                                                if(isset($total_price)){
                                                    $tva= (($total_price/100)*20);
                                                    $basket_price = $tva+$total_price;
                                                    $_SESSION['total_panier']=$basket_price;

                                                    if (isset($_POST['reduction']) && !empty($_POST['reduction'])){
                                                        $_SESSION['total_panier'] = ($basket_price-$reduction);

                                                        echo $_SESSION['total_panier']." €";
                                                    }
                                                    else
                                                    {
                                                        echo $basket_price." €";
                                                    }
                                                    
                                                    
                                                    
                                                }
                                                else
                                                {
                                                    echo "0";
                                                }
                                            ?>
                                        </span>
                            </li>
                        </ul>
                        <a href="../../traitements/traitement_commande.php?iWantToOrder=true" class="primary-btn">Regler via Paypal</a>
                                            <br/>
                        <a  id="checkout-button" class="primary-btn">Regler via Carte Bancaire</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Shoping Cart Section End -->

    <script type="text/javascript">
    console.log("paa");
        var panier = <?php echo $_SESSION['total_panier'];?>;
        var formData = new FormData();
        formData.append('panier', panier);
        console.log(panier);
        // Create an instance of the Stripe object with your publishable API key
        var stripe = Stripe("pk_test_51HLaEDC6tvxFIDbHeRKVQ9iYXhO5SgspSbHXw2BwZklunWY9qwtV18YBDye1MxmsocxarpqCgDdqTifyvJzeI1dC00iCqC1ndC");
        var checkoutButton = document.getElementById("checkout-button");
        checkoutButton.addEventListener("click", function () {
        fetch("create-checkout-session.php", {
            method: "POST",
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
            body: formData
        })
            .then(function (response) {
            return response.json();
            })
            .then(function (session) {
            return stripe.redirectToCheckout({ sessionId: session.id });
            })
            .then(function (result) {
            // If redirectToCheckout fails due to a browser or network
            // error, you should display the localized error message to your
            // customer using error.message.
            if (result.error) {
                alert(result.error.message);
            }
            })
            .catch(function (error) {
            console.error("Error sur le paiement via carte:", error);
            });
        });
    </script>

    


</body>

<?php include "../Commons/footer.php";?>

</html>