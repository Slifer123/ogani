<?php 
    //include "utile/config.php";

    function connexionPDO()
    {

        /*try{
            //$bdd = new PDO("mysql:host = ".HOST_NAME.";dbname =".DATABASE_NAME.";port = ".PORT.";charset = utf8",USER_NAME,PASSWORD);
            $bdd = new PDO("mysql:host = localhost;dbname = projet_codeur;port = 8889","root","root");
            $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

            var_dump($bdd);

            return $bdd;
       }
        catch(PDOException $e)
        {
            $message = "Erreur PDO avec le message "+$e->getMessage();
            die($message);
        }*/
                
                $dsn = 'mysql:dbname=' . DATABASE_NAME . ';host=' . HOST_NAME .';port=' . PORT;
				try 
				{
                    $bdd = new PDO($dsn, USER_NAME , PASSWORD ,array(\PDO::MYSQL_ATTR_INIT_COMMAND =>  'SET NAMES utf8'));
                    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
					//$this->bdd = $bdd;
					return $bdd;
				} 
				catch (PDOException $e) 
				{
                    $message =  'Connexion échouée : ' . $e->getMessage();
                    die($message);
				}


    }

?>
