<?php include "../Commons/header.php";?>

    <?php 
    if(isset($_POST['nom']) && !empty($_POST['nom']) && 
    isset($_POST['mail']) && !empty($_POST['mail']) &&
    isset($_POST['objet']) && !empty($_POST['objet']) &&
    isset($_POST['message']) && !empty($_POST['message']) &&
    isset($_POST['captcha']) && !empty($_POST['captcha'])
    ){
        $captcha = (int) $_POST['captcha'];
        if($captcha === 8){
            $nom = htmlentities($_POST['nom']);
            $mail = htmlentities($_POST['mail']);
            $objet = htmlentities($_POST['objet']);
            $message = htmlentities($_POST['message']);
            $destinataire = "kings@gmail.com";
            mail($destinataire, $objet. " - " . $nom, "Mail : ". $mail. " Message : " . $message);
            echo '<div class="alert alert-success" role="alert">';
                echo 'Message envoyé';
            echo '</div>';
        } else {
            echo '<div class="alert alert-danger" role="alert">';
            echo 'Erreur de Captcha, recommencer';
            echo '</div>';
        }
    }
    ?>

    <div class=" align-items-center center">
        <?= formatTitrePageH3("Mot de passe oublié", "") ?>
        <div class="col-md-4 offset-md-4">
            <form method="POST" action="#" class="mt-3 pt-3 pl-3 pr-3 pb-3 rounded perso_shadow">


                <div class="form-group row no-gutters align-items-center">
                    
                    <input type="email" name="mail" id="mail" class="form-control " Placeholder = "Email" required/>
                </div>

                

                
                <input type="submit" class="btn btn-primary mx-auto d-block" value="Valider" />

                
                    
                

            </form>
        </div>
    </div>




<?php include "../Commons/footer.php";?>